
import numpy as np
from scipy.stats import beta
import heapq
import random
import copy
import argparse
import matplotlib.pyplot as plt
import csv
import pandas as pd
import scipy.stats
import math
import operator


# --------------------------------------------------------------------------------------
# number of instances
K                = 20

# budget
budget      = 200

# cost for actions:
cost_single      = 1
cost_peer_list   = [2.5]

# number of experiments
N                = 400
# -------------------------


"""
total_ratio_list = []

for action_list in total_action_list:
    current_cost = 0
    tmp_strategy = []
    i = 0
    ratio_list = []
    for strategy in action_list:
        tmp_strategy.append(strategy)
        if strategy == 1:
            current_cost += 2.5
        else:
            current_cost += 1.0
        # print("tmp_strategy is:", tmp_strategy)
        if  current_cost > budget_block[i]:
            a = tmp_strategy.pop()
            ratio_list.append(tmp_strategy.count(1)/len(tmp_strategy))
            i+=1
            tmp_strategy = [a]

        if current_cost == budget:
            ratio_list.append(tmp_strategy.count(1)/len(tmp_strategy))
    total_ratio_list.append(ratio_list)


print("total_ratio_list is:", total_ratio_list)
print("lenth of total_ratio_list is:", len(total_ratio_list))
print("total_ratio_list[0]", total_ratio_list[0], len(total_ratio_list[0]))
mean_ratio = [np.mean([total_ratio_list[j][i] for j in range(N)]) for i in range(len(budget_block))]
print("mean_ratio is:", mean_ratio)
print(len(mean_ratio))
"""

budget_block = np.arange(5, budget+1, 5)

# peer worker strategy ratio: \theta_task ~ uniform [0.5, 1.0]; \alpha_p ~ norm [0.9, 0.01]; \alpha_s ~ norm [0.8, 0.01]; c_p = 2.5, c_s = 1.0, K = 20, Budget = 200, uniform correlated, lambda = 0.045
mean_ratio_8 = [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.98333333333333328, 0.94374999999999998, 0.88112499999999994, 0.77625, 0.67912499999999998, 0.56525000000000003, 0.45904166666666668, 0.42558333333333337, 0.40470833333333334, 0.41720833333333329, 0.4247083333333333, 0.40770833333333328, 0.40625, 0.41808333333333336, 0.40337499999999998, 0.3909583333333333, 0.40004166666666663, 0.38704166666666667, 0.39349999999999996, 0.38737499999999997, 0.39479166666666665, 0.39708333333333329, 0.39383333333333331, 0.40125, 0.39799999999999996, 0.40395833333333331, 0.40354166666666663, 0.40541666666666665]

# peer worker strategy ratio: \theta_task ~ uniform [0.5, 1.0]; \alpha_p ~ norm [0.9, 0.01]; \alpha_s ~ norm [0.7, 0.01]; c_p = 2.5, c_s = 1.0, K = 20, Budget = 200, uniform correlated, lambda = 0.040
mean_ratio_7 = [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.9970833333333334, 0.985, 0.9595833333333332, 0.9389583333333333, 0.8633333333333334, 0.796875, 0.7484166666666667, 0.7344583333333332, 0.68475, 0.6913333333333332, 0.6799166666666667, 0.6832499999999999, 0.690375, 0.7115416666666667, 0.7042083333333333, 0.712375, 0.71925, 0.7241249999999999, 0.7313333333333333, 0.7297916666666666, 0.7305833333333334, 0.7433333333333334, 0.7461249999999999, 0.7504166666666667]


# peer worker strategy ratio: \theta_task ~ uniform [0.5, 1.0]; \alpha_p ~ norm [0.9, 0.01]; \alpha_s ~ norm [0.6, 0.01]; c_p = 2.5, c_s = 1.0, K = 20, Budget = 200, uniform correlated, lambda = 0.035
mean_ratio_6 = [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.9970833333333334, 0.98875, 0.9741666666666666, 0.964375, 0.9527083333333334, 0.9270833333333335, 0.9327083333333333, 0.9184166666666667, 0.9064583333333333, 0.9050833333333332, 0.911875, 0.9187083333333332, 0.9189583333333334, 0.9084166666666667, 0.9221666666666667, 0.9180833333333334, 0.9283333333333335, 0.9219583333333332, 0.9241666666666666, 0.9235416666666667, 0.9221666666666667]

f = plt.figure()
plt.scatter(budget_block, mean_ratio_8, marker = "X", s=8, color = 'r', label = 'Low ability gap')
plt.scatter(budget_block, mean_ratio_7, marker = "o", s=8, color = 'b', label = 'Median ability gap')
plt.scatter(budget_block, mean_ratio_6, marker = "v", s=8, color = 'g', label = 'High ability gap')

plt.yticks([0.25, 0.50,  0.75, 1.00])
plt.ylabel("Peer Workers Strategy Ratio")
plt.xlabel("Budget spent")
plt.legend(loc = 'lower left', prop={'size': 9})

f.savefig("CMDP_peer_ratio_gap.pdf", bbox_inches='tight', dpi=1000)

