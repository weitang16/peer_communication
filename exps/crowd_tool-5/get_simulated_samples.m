function [sample_labels, sample_correlated_labels, sample_labels_peer_only, sample_correlated_labels_peer_only] = get_simulated_samples(budget, N)

% simulated samples.

% paras: budget: # of queries for single workers, peers, respectively
%        N     : # of task; the true labels in top N/2 tasks are 1; while the remaining are 2 
%        labels domain: 1, 2
    
    
    % generate random p_plus for single workers
    m = 0.75; s = 1;
    l = 0.5 * ones(1,N * budget); u = 1.0 * ones(1, N * budget);
    X = trandn((l-m)/s,(u-m)/s); 
    p_plus_single = vec2mat(m+s*X, budget);
        
    % generate random p_plus for peers
    p_plus_peer = p_plus_single(:, 1:budget/2) + 0.5 * (1 - p_plus_single(:, 1:budget/2)).*rand(size(p_plus_single(:, 1:budget/2)));
    
    % generate random p_zero for peers
    p_zero_peer = 0 + (1 - p_plus_peer).*rand(size(p_plus_peer));
    
    % get the p_minus for peers
    p_minus_peer = 1 - p_plus_peer - p_zero_peer;

    
    % get the simulated samples.
    sample_labels = zeros(N, 2 * budget);
    
    for task_index = 1: N
        if task_index <= N/2
            for query_index = 1:budget
                if rand(1) <=  p_plus_single(task_index, query_index)
                    sample_labels(task_index, query_index) = 1; 
                else
                    sample_labels(task_index, query_index) = 2;
                end
            end
        else
            for query_index = 1:budget
                if rand(1) <=  p_plus_single(task_index, query_index)
                    sample_labels(task_index, query_index) = 2; 
                else
                    sample_labels(task_index, query_index) = 1;
                end
            end   
        end
        
        
        if task_index <= N/2
            for query_index = 1:budget/2
                if rand(1) <=  p_plus_peer(task_index, query_index)
                    sample_labels(task_index, budget + query_index * 2 - 1) = 1; 
                    sample_labels(task_index, budget + query_index * 2)     = 1; 
                elseif rand(1) <=  p_minus_peer(task_index, query_index)
                    sample_labels(task_index, budget + query_index * 2 - 1) = 2; 
                    sample_labels(task_index, budget + query_index * 2)     = 2;
                else
                    sample_labels(task_index, budget + query_index * 2 - 1) = randsample([1,2], 1); 
                    sample_labels(task_index, budget + query_index * 2)     = 3 - sample_labels(task_index, budget + query_index * 2 - 1);
                end
            end
        else
            for query_index = 1:budget/2
                if rand(1) <=  p_plus_peer(task_index, query_index)
                    sample_labels(task_index, budget + query_index * 2 - 1) = 2; 
                    sample_labels(task_index, budget + query_index * 2)     = 2; 
                elseif rand(1) <=  p_minus_peer(task_index, query_index)
                    sample_labels(task_index, budget + query_index * 2 - 1) = 1; 
                    sample_labels(task_index, budget + query_index * 2)     = 1;
                else
                    sample_labels(task_index, budget + query_index * 2 - 1) = randsample([1,2], 1); 
                    sample_labels(task_index, budget + query_index * 2)     = 3 - sample_labels(task_index, budget + query_index * 2 - 1);
                end
            end   
        end
    end
    
    sample_correlated_labels = zeros(N, budget + budget/2);
    sample_correlated_labels(:, 1:budget) = sample_labels(:, 1:budget);
    for task_index = 1: N
        for index =  budget+1: budget + budget/2
            if sample_labels(task_index, ((index - budget)-1)*2 + budget + 1) == 1 && sample_labels(task_index, ((index - budget)-1)*2 + budget + 2) == 1
                sample_correlated_labels(task_index, index) = 1;
            elseif sample_labels(task_index, ((index - budget)-1)*2 + budget + 1) == 2 && sample_labels(task_index, ((index - budget)-1)*2 + budget + 2) == 2
                sample_correlated_labels(task_index, index) = 2;
            else
                sample_correlated_labels(task_index, index) = 0;
            end 
        end 
    end
    
    sample_labels_peer_only = zeros(N, budget);
    sample_labels_peer_only(:,1:budget) = sample_labels(:, budget+1: 2*budget);
    sample_correlated_labels_peer_only = zeros(N, budget/2);
    sample_correlated_labels_peer_only(:,1:budget/2) = sample_correlated_labels(:, budget+1: budget + budget/2);
    
end
    
    
    
    
    