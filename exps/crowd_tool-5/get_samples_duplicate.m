function [sample_labels, sample_correlated_labels, sample_labels_peer_only, sample_correlated_labels_peer_only] = get_samples_duplicate(budget, single_worker_label, peer_worker_label)
% duplicate the tasks

    k = 3;
    
    % get the samples from the real data. 
    sample_labels = zeros(20 * k, 2 * budget);
    for duplicate_index = 1:k
        for task_index = 1: 20
            task_list_single     = find(single_worker_label(:,1) == task_index);
            sample_index_single  =  randsample(task_list_single, budget, true);
            sample_labels(task_index + (duplicate_index-1)*20 , 1: budget) = single_worker_label(sample_index_single, 2);

            task_list_peer     = find(peer_worker_label(:,1) == task_index);
            sample_index_peer  =  randsample(task_list_peer, budget/2, true);
            sample_labels(task_index + (duplicate_index-1)*20, budget+1:2:budget*2-1) = peer_worker_label(sample_index_peer, 2);
            sample_labels(task_index + (duplicate_index-1)*20, budget+2:2:budget*2)   = peer_worker_label(sample_index_peer, 3);
        end
    end
    
    sample_correlated_labels = zeros(20 * k, budget + budget/2);
    sample_correlated_labels(:, 1:budget) = sample_labels(:, 1:budget);
    for duplicate_index = 1:k
        for task_index = 1: 20
            for index =  budget+1: budget + budget/2

                if sample_labels(task_index  + (duplicate_index-1)*20, ((index - budget)-1)*2 + budget + 1) == 1 && sample_labels(task_index, ((index - budget)-1)*2 + budget + 2) == 1
                    sample_correlated_labels(task_index + (duplicate_index-1)*20, index) = 1;
                elseif sample_labels(task_index + (duplicate_index-1)*20, ((index - budget)-1)*2 + budget + 1) == 2 && sample_labels(task_index, ((index - budget)-1)*2 + budget + 2) == 2
                    sample_correlated_labels(task_index, index) = 2;
                else
                    sample_correlated_labels(task_index + (duplicate_index-1)*20, index) = 0;
                end

            end
        end
    end
    
    sample_labels_peer_only = zeros(20 * k, budget);
    sample_labels_peer_only(:,1:budget) = sample_labels(:, budget+1: 2*budget);
    sample_correlated_labels_peer_only = zeros(20 * k, budget/2);
    sample_correlated_labels_peer_only(:,1:budget/2) = sample_correlated_labels(:, budget+1: budget + budget/2);
    
end
    
    
    
    
    