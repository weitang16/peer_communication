function [sample_labels, sample_correlated_labels, sample_labels_peer_only, sample_correlated_labels_peer_only] = get_samples(budget, single_worker_label, peer_worker_label)

% get samples from the real data.
% --paras start: 
%   label domain: 1: h; 2: m; 0: received no labels

    % get the samples from the real data. 
    sample_labels = zeros(20, 2 * budget);
    for task_index = 1: 20
        task_list_single     = find(single_worker_label(:,1) == task_index);
        sample_index_single  =  randsample(task_list_single, budget, true);
        sample_labels(task_index, 1: budget) = single_worker_label(sample_index_single, 2);

        task_list_peer     = find(peer_worker_label(:,1) == task_index);
        sample_index_peer  =  randsample(task_list_peer, budget/2, true);
        sample_labels(task_index, budget+1:2:budget*2-1) = peer_worker_label(sample_index_peer, 2);
        sample_labels(task_index, budget+2:2:budget*2)   = peer_worker_label(sample_index_peer, 3);
    end
    
    sample_correlated_labels = zeros(20, budget + budget/2);
    sample_correlated_labels(:, 1:budget) = sample_labels(:, 1:budget);
    for task_index = 1: 20
        for index =  budget+1: budget + budget/2
            
            if sample_labels(task_index, ((index - budget)-1)*2 + budget + 1) == 1 && sample_labels(task_index, ((index - budget)-1)*2 + budget + 2) == 1
                sample_correlated_labels(task_index, index) = 1;
            elseif sample_labels(task_index, ((index - budget)-1)*2 + budget + 1) == 2 && sample_labels(task_index, ((index - budget)-1)*2 + budget + 2) == 2
                sample_correlated_labels(task_index, index) = 2;
            else
                sample_correlated_labels(task_index, index) = 0;
            end
            
        end
        
        
    end
    
    sample_labels_peer_only = zeros(20, budget);
    sample_labels_peer_only(:,1:budget) = sample_labels(:, budget+1: 2*budget);
    sample_correlated_labels_peer_only = zeros(20, budget/2);
    sample_correlated_labels_peer_only(:,1:budget/2) = sample_correlated_labels(:, budget+1: budget + budget/2);
    
end
    
    
    
    
    