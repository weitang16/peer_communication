clear all
addpath(genpath(pwd));

% load single_worker.mat
% load peer_worker.mat

% ----- paras start
true_labels = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2];
budget_list = 1:1:10;
N           = 50; % # of experiments
N_tasks     = 20; % # of tasks 
% ----- paras end

avg_error_for_budget      = zeros(length(budget_list), 9);
avg_error_for_budget_corr = zeros(length(budget_list), 9);



for budget_index          = 1:length(budget_list)
    
    budget                  = budget_list(budget_index);
    error_list_for_exp      = zeros(N, 9);
    error_list_for_exp_corr = zeros(N, 9);
    
    for exp_index = 1:N
        [sample_labels, sample_labels_corr,sample_labels_peer_only, sample_labels_corr_peer_only] = get_simulated_samples(budget, N_tasks);
    
        % get the error rates without considering correlation: labels are
        % from a mixed of single workers and peers
        error_list = get_errors(sample_labels, true_labels);
        error_list_for_exp(exp_index, :) = error_list;
        
        % get the error rates with considering correlation: labels are from
        % a mixed of sinlge workers and peers
        error_list_corr = get_errors(sample_labels_corr, true_labels);
        error_list_for_exp_corr(exp_index, :) = error_list_corr;
        
        
        if mod(exp_index, 50) == 0
            fprintf('++++++++++ exp %d done \n', exp_index);
        end
    end
    
    avg_error_list = mean(error_list_for_exp, 1);
    avg_error_for_budget(budget_index, :) = avg_error_list;
    
    avg_error_list_corr = mean(error_list_for_exp_corr, 1);
    avg_error_for_budget_corr(budget_index, :) = avg_error_list_corr;
    
    
    fprintf('+++++++++++++++++++ budget %d done \n', budget);
end

save('results.mat', 'avg_error_for_budget', 'avg_error_for_budget_corr', 'avg_error_for_budget_corr_peer_only', 'avg_error_for_budget_peer_only');

linestyle = {'--or', '--+g', '--*b', '--.c', '--xm', '--sy', '--dk', '--^r', '--vb'};


% plots for aggregation on a mixed of single and peers
%---------------------------------------------------------
without_corr = figure('visible','off');
for i = 1:9
    plot(budget_list, avg_error_for_budget(:,i),linestyle{i}, 'LineWidth',1.5)
    hold on
end

% ylim([0.11, 0.21])
% set(gca, 'yTick',[0.11, 0.12, 0.14, 0.16, 0.18, 0.20, 0.21] );       
ylabel('Error rate')
xlabel('Budget: number of queries for single and peers, respectively')
title('Without Considering Correlation')
legend('MV', 'EM^2', 'EM^1', 'BP^1', 'BP^2', 'MF^1', 'MF^2', 'KOS_{karger}', 'KOS_{BP}');

saveas(gcf,'results_mixed_simulation.pdf')
hold off

%---------------------------------------------------------
with_corr = figure('visible','off');
for i = 1:9
    plot(budget_list, avg_error_for_budget_corr(:,i),linestyle{i}, 'LineWidth',1.5)
    hold on
end

% ylim([0.11, 0.21])
% set(gca, 'yTick',[0.11, 0.12, 0.14, 0.16, 0.18, 0.20, 0.21] );       
ylabel('Error rate')
xlabel('Budget: number of queries for single and peers, respectively')
title('With Considering Correlation')
legend('MV', 'EM^2', 'EM^1', 'BP^1', 'BP^2', 'MF^1', 'MF^2', 'KOS_{karger}', 'KOS_{BP}');

saveas(gcf,'results_corr_mixed_simulation.pdf')



%{
avg_error_for_budget_peer_only      = zeros(length(budget_list), 9);
avg_error_for_budget_corr_peer_only = zeros(length(budget_list), 9);

for budget_index          = 1:length(budget_list)
    
    budget                  = budget_list(budget_index);
    
    error_list_for_exp_peer_only      = zeros(N, 9);
    error_list_for_exp_corr_peer_only = zeros(N, 9);
    
    for exp_index = 1:N
        [sample_labels, sample_labels_corr,sample_labels_peer_only, sample_labels_corr_peer_only] = get_simulated_samples(budget, N_tasks);
        
        % get the error rates without considering correlation: labels are
        % only from peers
        error_list_peer_only = get_errors(sample_labels_peer_only, true_labels);
        error_list_for_exp_peer_only(exp_index, :) = error_list_peer_only;
        
        % get the error rates with considering correlation: labels are only
        % from peers
        error_list_corr_peer_only = get_errors(sample_labels_corr_peer_only, true_labels);
        error_list_for_exp_corr_peer_only(exp_index, :) = error_list_corr_peer_only;
        
        
        if mod(exp_index, 50) == 0
            fprintf('++++++++++ exp %d done \n', exp_index);
        end
    end
    
    avg_error_list_peer_only = mean(error_list_for_exp_peer_only, 1);
    avg_error_for_budget_peer_only(budget_index, :) = avg_error_list_peer_only;
    
    avg_error_list_corr_peer_only = mean(error_list_for_exp_corr_peer_only, 1);
    avg_error_for_budget_corr_peer_only(budget_index, :) = avg_error_list_corr_peer_only;
    
    fprintf('+++++++++++++++++++ budget %d done \n', budget);
end



% plots for aggregation on peer only
%---------------------------------------------------------
without_corr_peer_only = figure('visible','off');
for i = 1:9
    plot(budget_list, avg_error_for_budget_peer_only(:,i),linestyle{i}, 'LineWidth',1.5)
    hold on
end

ylim([0.11, 0.21])
set(gca, 'yTick',[0.11, 0.12, 0.14, 0.16, 0.18, 0.20, 0.21] );       
ylabel('Error rate')
xlabel('Budget: number of queries for peers, respectively')
title('Without Considering Correlation')
legend('MV', 'EM^2', 'EM^1', 'BP^1', 'BP^2', 'MF^1', 'MF^2', 'KOS_{karger}', 'KOS_{BP}');

saveas(gcf,'results_peer_only.pdf')
hold off

%---------------------------------------------------------
with_corr_peer_only = figure('visible','off');
for i = 1:9
    plot(budget_list, avg_error_for_budget_corr_peer_only(:,i),linestyle{i}, 'LineWidth',1.5)
    hold on
end

ylim([0.11, 0.21])
set(gca, 'yTick',[0.11, 0.12, 0.14, 0.16, 0.18, 0.20, 0.21] );       
ylabel('Error rate')
xlabel('Budget: number of queries peers, respectively')
title('With Considering Correlation')
legend('MV', 'EM^2', 'EM^1', 'BP^1', 'BP^2', 'MF^1', 'MF^2', 'KOS_{karger}', 'KOS_{BP}');

saveas(gcf,'results_corr_peer_only.pdf')

%}









