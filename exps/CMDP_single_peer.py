# -*- coding: UTF-8 -*-
# w.tang@wustl.edu 17/07 2018
# Solving Constrained MDP in CrowdSourcing
# Standard constrained MDP process on including the single workers and peer communication: tune the peer cost parameters
# usage:   python CMDP_single_peer.py -h
# example: python CMDP_single_peer.py -N 30 -K 50 -b 20 40 80 160 320 640 -c-p 1.1 1.8 1.9 2.0 2.1 2.2 4 -lambda 0.005 -peer-theta 0.1 0.99 -rho 0.8 0.99
#          -N           : number of experiments
#          -K           : number of Instances
#          -b           : budget list, int.
#          -c-s         : cost for single workers is set as default 1, but you can set as you want
#          -c-p         : a list of cost of peer workers,
#          -lambda      : lagrange multiplier
#          -peer-theta  : parameters for Peer theta Uniform Distribution: take two-element list as input, one is minimum value, one is the max value
#          -rho         : parameters for rho Uniform Distribution: take two-element list as input, one is minimum value, one is the max value
# ---------------------------------------------------------------------

import numpy as np
from scipy.stats import beta
import heapq
import random
import copy
import argparse
import matplotlib.pyplot as plt


# ----------
# functions:
def big_I(a, b):
    return 1 - beta.cdf(0.5, a, b, loc=0, scale=1)


def small_h(x):
    return max(x, 1 - x)


def exp_theta(a, b, c, d, z):
    if z == 1:
        return a * ((a + 1) * c + b * d) / ((a + b + 1) * (a * c + b * d))
    else:
        return a * ((a + 1) * d + b * c) / ((a + b + 1) * (b * c + a * d))


def exp_square_theta(a, b, c, d, z):
    if z == 1:
        return a * (a + 1) * ((a + 2) * c + b * d) / ((a + b + 1) * (a + b + 2) * (a * c + b * d))
    else:
        return a * (a + 1) * (b * c + (a + 2) * d) / ((a + b + 1) * (a + b + 2) * (b * c + a * d))


def exp_rho(a, b, c, d, z):
    if z == 1:
        return c * (a * (c + 1) + b * d) / ((c + d + 1) * (a * c + b * d))
    else:
        return c * (b * (c + 1) + a * d) / ((c + d + 1) * (b * c + a * d))


def exp_square_rho(a, b, c, d, z):
    if z == 1:
        return c * (c + 1) * (a * (c + 2) + b * d) / ((c + d + 1) * (c + d + 2) * (a * c + b * d))
    else:
        return c * (c + 1) * (b * (c + 2) + a * d) / ((c + d + 1) * (c + d + 2) * (b * c + a * d))


def a_tilde(a, b, c, d, z):
    return exp_theta(a, b, c, d, z) * (exp_theta(a, b, c, d, z) - exp_square_theta(a, b, c, d, z)) / (
    exp_square_theta(a, b, c, d, z) - exp_theta(a, b, c, d, z) ** 2)


def b_tilde(a, b, c, d, z):
    return (1 - exp_theta(a, b, c, d, z)) * (exp_theta(a, b, c, d, z) - exp_square_theta(a, b, c, d, z)) / (
    exp_square_theta(a, b, c, d, z) - exp_theta(a, b, c, d, z) ** 2)


def c_tilde(a, b, c, d, z):
    return exp_rho(a, b, c, d, z) * (exp_rho(a, b, c, d, z) - exp_square_rho(a, b, c, d, z)) / (
    exp_square_rho(a, b, c, d, z) - exp_rho(a, b, c, d, z) ** 2)


def d_tilde(a, b, c, d, z):
    return (1 - exp_rho(a, b, c, d, z)) * (exp_rho(a, b, c, d, z) - exp_square_rho(a, b, c, d, z)) / (
    exp_square_rho(a, b, c, d, z) - exp_rho(a, b, c, d, z) ** 2)


def R_single(a, b, c, d, z, lambda_cost):

    return small_h(big_I(a_tilde(a, b, c, d, z), b_tilde(a, b, c, d, z))) - small_h(big_I(a, b)) - lambda_cost * cost_single


def R_peer(a, b, c, d, z_1, z_2, lambda_cost):

    a_tilde_updated = a_tilde(a, b, c, d, z_1)
    b_tilde_updated = b_tilde(a, b, c, d, z_1)
    c_tilde_updated = c_tilde(a, b, c, d, z_1)
    d_tilde_updated = d_tilde(a, b, c, d, z_1)

    return small_h(big_I(a_tilde(a_tilde_updated, b_tilde_updated, c_tilde_updated, d_tilde_updated, z_2),
                         b_tilde(a_tilde_updated, b_tilde_updated, c_tilde_updated, d_tilde_updated, z_2))) - \
           small_h(big_I(a, b)) - lambda_cost * cost_peer


def twice_updating(a, b, c, d, z_1, z_2):

    a_tilde_updated_1 = a_tilde(a, b, c, d, z_1)
    b_tilde_updated_1 = b_tilde(a, b, c, d, z_1)
    c_tilde_updated_1 = c_tilde(a, b, c, d, z_1)
    d_tilde_updated_1 = d_tilde(a, b, c, d, z_1)

    a_tilde_updated_2 = a_tilde(a_tilde_updated_1, b_tilde_updated_1, c_tilde_updated_1, d_tilde_updated_1, z_2)
    b_tilde_updated_2 = b_tilde(a_tilde_updated_1, b_tilde_updated_1, c_tilde_updated_1, d_tilde_updated_1, z_2)
    c_tilde_updated_2 = c_tilde(a_tilde_updated_1, b_tilde_updated_1, c_tilde_updated_1, d_tilde_updated_1, z_2)
    d_tilde_updated_2 = d_tilde(a_tilde_updated_1, b_tilde_updated_1, c_tilde_updated_1, d_tilde_updated_1, z_2)

    return a_tilde_updated_2, b_tilde_updated_2, c_tilde_updated_2, d_tilde_updated_2


def R_single_benchmark(a,b ,c , d, z):

    return small_h(big_I(a_tilde(a, b, c, d, z), b_tilde(a, b, c, d, z))) - small_h(big_I(a, b))
# ----------

# --------------- benchmark: perfect single workers using same updating rule ------------
def MDP_benchmark(single_theta, peer_theta, cost_single, budget, K):

    """Get the performance of benchmark.

        Args:
            peer_theta  : a list of abilities of single workers, used to get the groundtruth of the instances
            single_theta: a list of abilities of single workers
            cost_single : cost of every single workers
            budget      : total budget, in this case, is the time budget
            K           : number of instances

        Returns:
            accuracy
    """

    # positive set:
    H_positive = []
    H_negative = []
    for index, i in enumerate(peer_theta):
        if i >= 0.5:
            H_positive.append(index)
        else:
            H_negative.append(index)
    # print("positive set is:", H_positive)

    instance_stat_single_benchmark = [[1, 1] for _ in range(K)]
    rho_stat_single_benchmark      = [[4, 1] for _ in range(K)]

    current_budget = 0
    while current_budget < budget:

        R_plus_list = []
        for k in range(K):
            R_plus = max(R_single_benchmark(instance_stat_single_benchmark[k][0], instance_stat_single_benchmark[k][1],
                                            rho_stat_single_benchmark[k][0], rho_stat_single_benchmark[k][1], 1),
                         R_single_benchmark(instance_stat_single_benchmark[k][0], instance_stat_single_benchmark[k][1],
                                            rho_stat_single_benchmark[k][0], rho_stat_single_benchmark[k][1], 0))
            R_plus_list.append(R_plus)

        selected_instance = R_plus_list.index(max(R_plus_list))

        tmp_instance_theta_stat = copy.deepcopy(instance_stat_single_benchmark)
        tmp_rho_stat_single = copy.deepcopy(rho_stat_single_benchmark)

        z = np.random.binomial(size=1, n=1, p=single_theta[selected_instance])

        instance_stat_single_benchmark[selected_instance][0] = a_tilde(tmp_instance_theta_stat[selected_instance][0],
                                                                       tmp_instance_theta_stat[selected_instance][1],
                                                                       tmp_rho_stat_single[selected_instance][0],
                                                                       tmp_rho_stat_single[selected_instance][1],
                                                                       z)
        instance_stat_single_benchmark[selected_instance][1] = b_tilde(tmp_instance_theta_stat[selected_instance][0],
                                                                       tmp_instance_theta_stat[selected_instance][1],
                                                                       tmp_rho_stat_single[selected_instance][0],
                                                                       tmp_rho_stat_single[selected_instance][1],
                                                                       z)

        rho_stat_single_benchmark[selected_instance][0] = c_tilde(tmp_instance_theta_stat[selected_instance][0],
                                                                  tmp_instance_theta_stat[selected_instance][1],
                                                                  tmp_rho_stat_single[selected_instance][0],
                                                                  tmp_rho_stat_single[selected_instance][1],
                                                                  z)

        rho_stat_single_benchmark[selected_instance][1] = d_tilde(tmp_instance_theta_stat[selected_instance][0],
                                                                  tmp_instance_theta_stat[selected_instance][1],
                                                                  tmp_rho_stat_single[selected_instance][0],
                                                                  tmp_rho_stat_single[selected_instance][1],
                                                                  z)

        current_budget += cost_single

    H_get_positive = []
    H_get_negative = []
    for index in range(K):
        if instance_stat_single_benchmark[index][0] >= instance_stat_single_benchmark[index][1]:
            H_get_positive.append(index)
        else:
            H_get_negative.append(index)

    accuracy = (len(set(H_get_positive) & set(H_positive)) + len(set(H_get_negative) & set(H_negative))) / K
    return accuracy
# ---------------------------------------------------------------------------------------

# --------------------------------- constrained MDP -------------------------------------
def constrained_MDP(single_theta, peer_theta, cost_single, cost_peer, lagrange_lambda, budget, K):

    """Get the performance of constrained MDP.

        Args:
            single_theta   : a list of abilities for single workers
            peer_theta     : a list of abilities for peer workers
            cost_single    : cost of every single workers
            cost_peer      : cost of every peer workers
            budget         : total budget, in this case, is the time budget
            lagrange_lambda: lagrange multiplier
            K              : number of instances

        Returns:
            accuracy
    """

    # positive set:
    H_positive = []
    H_negative = []
    for index, i in enumerate(peer_theta):
        if i >= 0.5:
            H_positive.append(index)
        else:
            H_negative.append(index)
    # print("positive set  is:", H_positive)

    instance_theta_stat = [[1, 1] for _ in range(K)]
    rho_stat_single     = [[4, 1] for _ in range(K)]
    rho_stat_peer       = [[10000000, 1] for _ in range(K)]  # we have knowledge on this parameter.

    action_taken        = []

    current_budget = 0
    while current_budget < budget:

        R_plus_list = [[0 for _ in range(2)] for _ in range(K)]
        for k in range(K):
            # single worker:
            R_plus_single = max(
                R_single(instance_theta_stat[k][0], instance_theta_stat[k][1], rho_stat_single[k][0],
                         rho_stat_single[k][1], 1, lagrange_lambda),
                R_single(instance_theta_stat[k][0], instance_theta_stat[k][1], rho_stat_single[k][0],
                         rho_stat_single[k][1], 0, lagrange_lambda))

            # peer communication
            R_plus_peer = max(
                R_peer(instance_theta_stat[k][0], instance_theta_stat[k][1], rho_stat_peer[k][0], rho_stat_peer[k][1],
                       1, 1, lagrange_lambda),
                R_peer(instance_theta_stat[k][0], instance_theta_stat[k][1], rho_stat_peer[k][0], rho_stat_peer[k][1],
                       0, 1, lagrange_lambda),
                R_peer(instance_theta_stat[k][0], instance_theta_stat[k][1], rho_stat_peer[k][0], rho_stat_peer[k][1],
                       1, 0, lagrange_lambda),
                R_peer(instance_theta_stat[k][0], instance_theta_stat[k][1], rho_stat_peer[k][0], rho_stat_peer[k][1],
                       0, 0, lagrange_lambda))

            # -------------------------------------------

            R_plus_list[k][0] = R_plus_single
            R_plus_list[k][1] = R_plus_peer

        R_plus_array = np.array(R_plus_list)
        selected_instance = np.where(R_plus_array == R_plus_array.max())[0][0]
        selected_labelling_strategy = np.where(R_plus_array == R_plus_array.max())[1][0]

        action_taken.append(selected_labelling_strategy)

        if selected_labelling_strategy == 0:  # single worker strategy

            tmp_instance_theta_stat = copy.deepcopy(instance_theta_stat)
            tmp_rho_stat_single = copy.deepcopy(rho_stat_single)

            z = np.random.binomial(size=1, n=1, p=single_theta[selected_instance])

            instance_theta_stat[selected_instance][0] = a_tilde(tmp_instance_theta_stat[selected_instance][0],
                                                                tmp_instance_theta_stat[selected_instance][1],
                                                                tmp_rho_stat_single[selected_instance][0],
                                                                tmp_rho_stat_single[selected_instance][1],
                                                                z)
            instance_theta_stat[selected_instance][1] = b_tilde(tmp_instance_theta_stat[selected_instance][0],
                                                                tmp_instance_theta_stat[selected_instance][1],
                                                                tmp_rho_stat_single[selected_instance][0],
                                                                tmp_rho_stat_single[selected_instance][1],
                                                                z)

            rho_stat_single[selected_instance][0] = c_tilde(tmp_instance_theta_stat[selected_instance][0],
                                                            tmp_instance_theta_stat[selected_instance][1],
                                                            tmp_rho_stat_single[selected_instance][0],
                                                            tmp_rho_stat_single[selected_instance][1],
                                                            z)

            rho_stat_single[selected_instance][1] = d_tilde(tmp_instance_theta_stat[selected_instance][0],
                                                            tmp_instance_theta_stat[selected_instance][1],
                                                            tmp_rho_stat_single[selected_instance][0],
                                                            tmp_rho_stat_single[selected_instance][1],
                                                            z)

            # print("instance_theta_stat is:", instance_theta_stat)
            # print("rho_stat_single     is:", rho_stat_single)
            # print("--------------------------------------------")

            current_budget += cost_single

        if selected_labelling_strategy == 1:  # peer worker strategy

            tmp_instance_theta_stat_peer = copy.deepcopy(instance_theta_stat)
            tmp_rho_stat_peer = copy.deepcopy(rho_stat_peer)

            [z_1, z_2] = np.random.binomial(size=2, n=1, p=peer_theta[selected_instance])
            # Method 1
            instance_theta_stat[selected_instance][0] = \
            twice_updating(tmp_instance_theta_stat_peer[selected_instance][0],
                           tmp_instance_theta_stat_peer[selected_instance][1],
                           tmp_rho_stat_peer[selected_instance][0],
                           tmp_rho_stat_peer[selected_instance][1],
                           z_1, z_2)[0]
            instance_theta_stat[selected_instance][1] = \
            twice_updating(tmp_instance_theta_stat_peer[selected_instance][0],
                           tmp_instance_theta_stat_peer[selected_instance][1],
                           tmp_rho_stat_peer[selected_instance][0],
                           tmp_rho_stat_peer[selected_instance][1],
                           z_1, z_2)[1]
            rho_stat_peer[selected_instance][0] = twice_updating(tmp_instance_theta_stat_peer[selected_instance][0],
                                                                 tmp_instance_theta_stat_peer[selected_instance][1],
                                                                 tmp_rho_stat_peer[selected_instance][0],
                                                                 tmp_rho_stat_peer[selected_instance][1],
                                                                 z_1, z_2)[2]

            rho_stat_peer[selected_instance][1] = twice_updating(tmp_instance_theta_stat_peer[selected_instance][0],
                                                                 tmp_instance_theta_stat_peer[selected_instance][1],
                                                                 tmp_rho_stat_peer[selected_instance][0],
                                                                 tmp_rho_stat_peer[selected_instance][1],
                                                                 z_1, z_2)[3]

            # print("instance_theta_stat     is:", instance_theta_stat)
            # print("rho_stat_peer           is:", rho_stat_peer)

            current_budget += cost_peer

    H_get_positive = []
    H_get_negative = []
    for index in range(K):
        if instance_theta_stat[index][0] >= instance_theta_stat[index][1]:
            H_get_positive.append(index)
        else:
            H_get_negative.append(index)

    accuracy = (len(set(H_get_positive) & set(H_positive)) + len(set(H_get_negative) & set(H_negative))) / K

    return accuracy, action_taken
# ---------------------------------------------------------------------------------------

# -----------------------------------------------------------

if __name__=="__main__":

    parser = argparse.ArgumentParser(description='CMDP Parameters.')

    parser.add_argument("--number-of-experiments",  "-N",  required=True, type=int, help="Number of Experiments")
    parser.add_argument("--number-of-instances",    "-K",  required=True, type=int, help="Number of Instances")
    parser.add_argument("--budget-list",            "-b",  nargs='+',     required=True,  type=int, help="Budget List")
    parser.add_argument("--cost-single",            "-c-s",type = float, default=1,
                        help="Cost of Single Workers: defoult value is 1",)
    parser.add_argument("--cost-peer-list",         "-c-p",nargs='+', required=True, type = float, help="Cost List of Peer Workers",)
    parser.add_argument("--lagrange-lambda",        "-lambda",  required=True, type = float, help="Lagrange Lambda Value",)
    parser.add_argument("--peer-theta-distribution","-peer-theta",nargs='+', required=True, type = float,
                        help="Parameters for Peer theta Uniform Distribution: take two-element list as input, one is minimum value, one is the max value",)
    parser.add_argument("--rho-distribution",       "-rho",  nargs='+', required=True, type = float,
                        help="Parameters for rho Uniform Distribution: take two-element list as input, one is minimum value, one is the max value",)

    args = parser.parse_args()

    # number of experiments
    N                = args.number_of_experiments

    # number of instances
    K                = args.number_of_instances

    # budget
    budget_list      = args.budget_list

    # cost for actions:
    cost_single      = args.cost_single
    cost_peer_list   = args.cost_peer_list

    # Lagrangian multiplier
    lagrange_lambda  = args.lagrange_lambda

    # parameters for peers and single workers
    d_peer = args.peer_theta_distribution[0]; u_peer =  args.peer_theta_distribution[1]
    d_rho  = args.rho_distribution[0]       ; u_rho  =  args.rho_distribution[1]

    print("-----------------------------------Parameters Start")
    print("number of experiments  :", N)
    print("number of instances    :", K)
    print("busget list            :", budget_list)
    print("cost for single workers:", cost_single)
    print("cost for peer workers  :", cost_peer_list)
    print("lambda is              :", lagrange_lambda)
    print("peer_theta_list are    :", "uniform distributed [%.2f, %.2f]" %(d_peer, u_peer))
    print("rho_list are           :", "uniform distributed [%.2f, %.2f]" %(d_rho,  u_rho))
    print("-----------------------------------Parameters End")

    # -----------------------------------------------------------
    # get the results from Constrained MDP

    single_theta_list  = []
    peer_theta_list    = []
    rho_list           = []

    accuracy_CMDP_list = []
    for exp in range(1, N+1):

        peer_theta   = [float("%.4f" % random.uniform(d_peer, u_peer)) for i in range(K)]
        rho          = [float("%.4f" % random.uniform(d_rho, u_rho)) for j in range(K)]
        single_theta = [float("%.4f" % (peer_theta[i] * rho[i] + (1 - peer_theta[i]) * (1 - rho[i]))) for i in range(K)]
        peer_theta_list.append(peer_theta)
        rho_list.append(rho)
        single_theta_list.append(single_theta)

        accuracy_CMDP = []
        for cost_peer in cost_peer_list:
            accuracy = []
            for budget in budget_list:
                accuracy.append(constrained_MDP(single_theta, peer_theta, cost_single, cost_peer, lagrange_lambda, budget, K)[0])

            accuracy_CMDP.append(accuracy)
            print("c_p -- %.2f is done." % cost_peer)
        accuracy_CMDP_list.append(accuracy_CMDP)
        print("CMDP exp ---- %d is done." % exp)

    avg_accuracy_CMDP_list = []
    for i in range(len(cost_peer_list)):
        avg_accuracy_CMDP_list.append([sum([accuracy_CMDP_list[k][i][j] for k in range(N)])/N for j in range(len(budget_list))])
    print("avg_accuracy_CMDP_list are:", avg_accuracy_CMDP_list)
    # -----------------------------------------------------------

    # -----------------------------------------------------------
    # get the results from MDP benchamrk
    accuracy_benchmark_list = []
    for exp in range(1, N+1):

        peer_theta    = peer_theta_list[exp-1]
        single_theta  = single_theta_list[exp-1]
        accuracy_benchmark = []
        for budget in budget_list:
            accuracy_benchmark.append(MDP_benchmark(single_theta, peer_theta, cost_single, budget, K))

        print("benchmark exp ---- %d is done." %exp)
        accuracy_benchmark_list.append(accuracy_benchmark)

    avg_accuracy_benchmark = []
    for i in range(len(budget_list)):
        avg_accuracy_benchmark.append(sum([accuracy_benchmark_list[j][i] for j in range(N)])/N)
    print("avg_accuracy_benchmark are:", avg_accuracy_benchmark)
    # -----------------------------------------------------------

    print("peer_theta_list are  : ", peer_theta_list)
    print("rho_list are         :", rho_list)
    print("single_theta_list are:", single_theta_list)

    plt.figure(1)

    for i in range(len(cost_peer_list)):
        plt.plot(range(1, len(budget_list) + 1), avg_accuracy_CMDP_list[i], label='c_p = ' + str(cost_peer_list[i]))
    plt.plot(range(1, len(budget_list) + 1), avg_accuracy_benchmark, label='benchmark')

    plt.xticks(range(1, len(budget_list) + 1), budget_list)
    plt.ylabel("Accuracy")
    plt.xlabel("Total budget")
    plt.legend()
    plt.title("CMDP(c_s = " + str(cost_single) + r', $\lambda = $' + str(lagrange_lambda) + ", K = " + str(K) + ", N = " + str(N) + ")" +
              r', $\theta_p: [$' + str(d_peer) + "," + str(u_peer) + "]" +
              r', $\rho: [$' + str(d_rho) + "," + str(u_rho) + "]")
    plt.savefig("CMDP(c_s = " + str(cost_single) + ', lambda = ' + str(lagrange_lambda) + ", K = " + str(K) + ", N = " + str(N) + ")" +
                ", peers theta uniformed[" + str(d_peer) + "," + str(u_peer) +"]" + ", rho unifomed[" + str(d_peer) + "," + str(u_peer) +"]" + ".png" )

    plt.show()

