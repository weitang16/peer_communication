# -*- coding: UTF-8 -*-
# w.tang@wustl.edu 17/07 2018
# applying Constrained MDP in CrowdSourcing on real data
# Constrained MDP process on including the single workers and peer communication: tune the peer cost parameters
# usage:   python CMDP_real_data_task_1_2.py -h
# example: python CMDP_real_data_task_1_2.py -N 10 -K 20 -b 20 40 60 80 -c-p 1.6 2.0 -lambda 0.01
#          -N               : number of experiments
#          -K               : number of Instances
#          -b               : budget list, int.
#          -c-s             : cost for single workers is set as default 1, but you can set as you want
#          -c-p             : a list of cost of peer workers,
#          -lambda          : lagrange multiplier
# Note: !!! This is particularly for task 1 and task 2 !!!
# ---------------------------------------------------------------------


import numpy as np
import scipy.stats
import heapq
import random
import copy
import argparse
import matplotlib.pyplot as plt
import csv
import pandas as pd
from scipy.stats import beta


def big_I(a, b):
    return 1 - beta.cdf(0.5, a, b, loc=0, scale=1)


def small_h(x):
    return max(x, 1 - x)

# moment of beta distribution
def k_order(k, a, b):

    return scipy.stats.beta.moment(k, a, b, loc=0, scale=1)


# ----------
def exp_theta(a, b, c, d, z):
    if z == 1:
        return a * ((a + 1) * c + b * d) / ((a + b + 1) * (a * c + b * d))
    else:
        return a * ((a + 1) * d + b * c) / ((a + b + 1) * (b * c + a * d))


def exp_square_theta(a, b, c, d, z):
    if z == 1:
        return a * (a + 1) * ((a + 2) * c + b * d) / ((a + b + 1) * (a + b + 2) * (a * c + b * d))
    else:
        return a * (a + 1) * (b * c + (a + 2) * d) / ((a + b + 1) * (a + b + 2) * (b * c + a * d))


def exp_rho(a, b, c, d, z):
    if z == 1:
        return c * (a * (c + 1) + b * d) / ((c + d + 1) * (a * c + b * d))
    else:
        return c * (b * (c + 1) + a * d) / ((c + d + 1) * (b * c + a * d))


def exp_square_rho(a, b, c, d, z):
    if z == 1:
        return c * (c + 1) * (a * (c + 2) + b * d) / ((c + d + 1) * (c + d + 2) * (a * c + b * d))
    else:
        return c * (c + 1) * (b * (c + 2) + a * d) / ((c + d + 1) * (c + d + 2) * (b * c + a * d))


def a_tilde(a, b, c, d, z):
    return exp_theta(a, b, c, d, z) * (exp_theta(a, b, c, d, z) - exp_square_theta(a, b, c, d, z)) / (
    exp_square_theta(a, b, c, d, z) - exp_theta(a, b, c, d, z) ** 2)


def b_tilde(a, b, c, d, z):
    return (1 - exp_theta(a, b, c, d, z)) * (exp_theta(a, b, c, d, z) - exp_square_theta(a, b, c, d, z)) / (
    exp_square_theta(a, b, c, d, z) - exp_theta(a, b, c, d, z) ** 2)


def c_tilde(a, b, c, d, z):
    return exp_rho(a, b, c, d, z) * (exp_rho(a, b, c, d, z) - exp_square_rho(a, b, c, d, z)) / (
    exp_square_rho(a, b, c, d, z) - exp_rho(a, b, c, d, z) ** 2)


def d_tilde(a, b, c, d, z):
    return (1 - exp_rho(a, b, c, d, z)) * (exp_rho(a, b, c, d, z) - exp_square_rho(a, b, c, d, z)) / (
    exp_square_rho(a, b, c, d, z) - exp_rho(a, b, c, d, z) ** 2)


def R_single(a, b, c, d, z, lambda_cost):

    return small_h(big_I(a_tilde(a, b, c, d, z), b_tilde(a, b, c, d, z))) - small_h(big_I(a, b)) - lambda_cost * cost_single


def R_peer(a, b, c, d, z_1, z_2, lambda_cost):

    a_tilde_updated = a_tilde(a, b, c, d, z_1)
    b_tilde_updated = b_tilde(a, b, c, d, z_1)
    c_tilde_updated = c_tilde(a, b, c, d, z_1)
    d_tilde_updated = d_tilde(a, b, c, d, z_1)

    return small_h(big_I(a_tilde(a_tilde_updated, b_tilde_updated, c_tilde_updated, d_tilde_updated, z_2),
                         b_tilde(a_tilde_updated, b_tilde_updated, c_tilde_updated, d_tilde_updated, z_2))) - \
           small_h(big_I(a, b)) - lambda_cost * cost_peer


def twice_updating(a, b, c, d, z_1, z_2):

    a_tilde_updated_1 = a_tilde(a, b, c, d, z_1)
    b_tilde_updated_1 = b_tilde(a, b, c, d, z_1)
    c_tilde_updated_1 = c_tilde(a, b, c, d, z_1)
    d_tilde_updated_1 = d_tilde(a, b, c, d, z_1)

    a_tilde_updated_2 = a_tilde(a_tilde_updated_1, b_tilde_updated_1, c_tilde_updated_1, d_tilde_updated_1, z_2)
    b_tilde_updated_2 = b_tilde(a_tilde_updated_1, b_tilde_updated_1, c_tilde_updated_1, d_tilde_updated_1, z_2)
    c_tilde_updated_2 = c_tilde(a_tilde_updated_1, b_tilde_updated_1, c_tilde_updated_1, d_tilde_updated_1, z_2)
    d_tilde_updated_2 = d_tilde(a_tilde_updated_1, b_tilde_updated_1, c_tilde_updated_1, d_tilde_updated_1, z_2)

    return a_tilde_updated_2, b_tilde_updated_2, c_tilde_updated_2, d_tilde_updated_2


def R_single_benchmark(a,b ,c , d, z):

    return small_h(big_I(a_tilde(a, b, c, d, z), b_tilde(a, b, c, d, z))) - small_h(big_I(a, b))


# MLE aggregation:
def R_peer_MLE(a, b, c, d, z, lambda_cost, cost_peer):

    return small_h(big_I(a_tilde(a, b, c, d, z), b_tilde(a, b, c, d, z))) - small_h(big_I(a, b)) - lambda_cost * cost_peer
# ----------




#-----------------------------------------------------------
# probability on observing the two labels
def observ_prob_nocov(a, b, c, d, z_1, z_2):

    if   z_1 == 1 and z_2 == 1:
        prob = 4 * k_order(2, a, b) * k_order(2, c, d) - 4 * k_order(2, a, b) * k_order(1, c, d) - 4 * k_order(1, a, b)* k_order(2, c, d) + \
               k_order(2, a, b) + k_order(2, c, d) + 6 * k_order(1, a, b) * k_order(1, c, d) - 2 * (k_order(1, a, b) + k_order(1, c, d)) + 1
        return prob

    elif z_1 == 1 and z_2 == 0:
        prob = - 4 * k_order(2, a, b) * k_order(2, c, d) + 4 * k_order(2, a, b) * k_order(1, c, d) + 4 * k_order(1, a, b) * k_order( 2, c, d) - \
               k_order(2, a, b) - k_order(2, c, d) - 4 * k_order(1, a, b) * k_order(1, c, d) + k_order(1, a, b) + k_order(1, c, d)
        return prob

    elif z_1 == 0 and z_2 == 1:
        prob = - 4 * k_order(2, a, b) * k_order(2, c, d) + 4 * k_order(2, a, b) * k_order(1, c, d) + 4 * k_order(1, a, b) * k_order(2, c, d) - \
               k_order(2, a, b) - k_order(2, c, d) - 4 * k_order(1, a, b) * k_order(1, c, d) + k_order(1, a, b) + k_order(1, c, d)
        return prob

    else:
        prob = 4 * k_order(2, a, b) * k_order(2, c, d) - 4 * k_order(2, a, b) * k_order(1, c, d) - 4 * k_order(1, a, b) * k_order(2, c, d) + \
               k_order(2, a, b) + k_order(2, c, d) + 2 * k_order(1, a, b) * k_order(1, c, d)
        return prob


def exp_theta_independence_nocov(a, b, c, d, z_1, z_2):

    if z_1 == 1 and z_2 == 1:
        numerator = (k_order(3, a, b) * (4 * k_order(2, c, d) - 4 * k_order(1, c, d) + 1) +
                     k_order(2, a, b) * (-2 * k_order(2, c, d) + 3 * k_order(1, c, d) - 1) * 2 +
                     k_order(1, a, b) * (k_order(2, c, d) - 2 * k_order(1, c, d) + 1))
        return numerator / (observ_prob_nocov(a, b, c, d, 1, 1))

    elif z_1 == 1 and z_2 == 0:
        numerator = (- k_order(3, a, b) * (4 * k_order(2, c, d) - 4 * k_order(1, c, d) + 1) +
                       k_order(2, a, b) * (4 * k_order(2, c, d) - 4 * k_order(1, c, d) + 1) +
                       k_order(1, a, b) * (-1 * k_order(2, c, d) + k_order(1, c, d)))
        return numerator/ (observ_prob_nocov(a, b, c, d, 1, 0))

    elif z_1 == 0 and z_2 == 1:
        numerator = (- k_order(3, a, b) * (4 * k_order(2, c, d) - 4 * k_order(1, c, d) + 1) +
                       k_order(2, a, b) * (4 * k_order(2, c, d) - 4 * k_order(1, c, d) + 1) +
                       k_order(1, a, b) * (-1 * k_order(2, c, d) + k_order(1, c, d)))
        return numerator/ (observ_prob_nocov(a, b, c, d, 0, 1))

    else:
        numerator = (k_order(3, a, b) * (4 * k_order(2, c, d) - 4 * k_order(1, c, d) + 1) +
                     k_order(2, a, b) * (-2 * k_order(2, c, d) + k_order(1, c, d)) * 2 +
                     k_order(1, a, b) * k_order(2, c, d))
        return numerator / (observ_prob_nocov(a, b, c, d, 0, 0))


def exp_square_theta_independence_nocov(a, b, c, d, z_1, z_2):

    if z_1 == 1 and z_2 == 1:
        numerator = (k_order(4, a, b) * (4 * k_order(2, c, d) - 4 * k_order(1, c, d) + 1) +
                     k_order(3, a, b) * (-2 * k_order(2, c, d) + 3 * k_order(1, c, d) - 1) * 2 +
                     k_order(2, a, b) * (k_order(2, c, d) - 2 * k_order(1, c, d) + 1))
        return numerator /(observ_prob_nocov(a, b, c, d, 1, 1))

    elif z_1 == 1 and z_2 == 0:
        numerator = (- k_order(4, a, b) * (4 * k_order(2, c, d) - 4 * k_order(1, c, d) + 1) +
                       k_order(3, a, b) * (4 * k_order(2, c, d) - 4 * k_order(1, c, d) + 1) +
                       k_order(2, a, b) * (-1 * k_order(2, c, d) + k_order(1, c, d)))
        return numerator / (observ_prob_nocov(a, b, c, d, 1, 0))

    elif z_1 == 0 and z_2 == 1:
        numerator = (- k_order(4, a, b) * (4 * k_order(2, c, d) - 4 * k_order(1, c, d) + 1) +
                       k_order(3, a, b) * (4 * k_order(2, c, d) - 4 * k_order(1, c, d) + 1) +
                       k_order(2, a, b) * (-1 * k_order(2, c, d) + k_order(1, c, d)))
        return numerator / (observ_prob_nocov(a, b, c, d, 0, 1))

    else:
        numerator = (k_order(4, a, b) * (4 * k_order(2, c, d) - 4 * k_order(1, c, d) + 1) +
                     k_order(3, a, b) * (-2 * k_order(2, c, d) + k_order(1, c, d)) * 2 +
                     k_order(2, a, b) * k_order(2, c, d))
        return numerator / (observ_prob_nocov(a, b, c, d, 0, 0))


def exp_rho_independence_nocov(a, b, c, d, z_1, z_2):

    if z_1 == 1 and z_2 == 1:
        numerator = (k_order(3, c, d) * (4 * k_order(2, a, b) - 4 * k_order(1, a, b) + 1) +
                     k_order(2, c, d) * (-2 * k_order(2, a, b) + 3 * k_order(1, a, b) - 1) * 2 +
                     k_order(1, c, d) * (k_order(2, a, b) - 2 * k_order(1, a, b) + 1))
        return numerator / (observ_prob_nocov(a, b, c, d, 1, 1))

    elif z_1 == 1 and z_2 == 0:
        numerator = (- k_order(3, c, d) * (4 * k_order(2, a, b) - 4 * k_order(1, a, b) + 1) +
                       k_order(2, c, d) * (4 * k_order(2, a, b) - 4 * k_order(1, a, b) + 1) +
                       k_order(1, c, d) * (-1 * k_order(2, a, b) + k_order(1, a, b)))
        return numerator / (observ_prob_nocov(a, b, c, d, 1, 0))

    elif z_1 == 0 and z_2 == 1:
        numerator = (- k_order(3, c, d) * (4 * k_order(2, a, b) - 4 * k_order(1, a, b) + 1) +
                     k_order(2, c, d) * (4 * k_order(2, a, b) - 4 * k_order(1, a, b) + 1) +
                     k_order(1, c, d) * (-1 * k_order(2, a, b) + k_order(1, a, b)))
        return numerator / (observ_prob_nocov(a, b, c, d, 0, 1))

    else:
        numerator = (k_order(3, c, d) * (4 * k_order(2, a, b) - 4 * k_order(1, a, b) + 1) +
                     k_order(2, c, d) * (-2 * k_order(2, a, b) + k_order(1, a, b)) * 2 +
                     k_order(1, c, d) * k_order(2, a, b))
        return numerator / (observ_prob_nocov(a, b, c, d, 0, 0))


def exp_square_rho_independence_nocov(a, b, c, d, z_1, z_2):

    if z_1 == 1 and z_2 == 1:
        numerator = (k_order(4, c, d) * (4 * k_order(2, a, b) - 4 * k_order(1, a, b) + 1) +
                     k_order(3, c, d) * (-2 * k_order(2, a, b) + 3 * k_order(1, a, b) - 1) * 2 +
                     k_order(2, c, d) * (k_order(2, a, b) - 2 * k_order(1, a, b) + 1))
        return numerator / (observ_prob_nocov(a, b, c, d, 1, 1))

    elif z_1 == 1 and z_2 == 0:
        numerator = (- k_order(4, c, d) * (4 * k_order(2, a, b) - 4 * k_order(1, a, b) + 1) +
                       k_order(3, c, d) * (4 * k_order(2, a, b) - 4 * k_order(1, a, b) + 1) +
                       k_order(2, c, d) * (-1 * k_order(2, a, b) + k_order(1, a, b)))
        return numerator / (observ_prob_nocov(a, b, c, d, 1, 0))

    elif z_1 == 0 and z_2 == 1:
        numerator = (- k_order(4, c, d) * (4 * k_order(2, a, b) - 4 * k_order(1, a, b) + 1) +
                       k_order(3, c, d) * (4 * k_order(2, a, b) - 4 * k_order(1, a, b) + 1) +
                       k_order(2, c, d) * (-1 * k_order(2, a, b) + k_order(1, a, b)))
        return numerator / (observ_prob_nocov(a, b, c, d, 0, 1))

    else:
        numerator = (k_order(4, c, d) * (4 * k_order(2, a, b) - 4 * k_order(1, a, b) + 1) +
                     k_order(3, c, d) * (-2 * k_order(2, a, b) + k_order(1, a, b)) * 2 +
                     k_order(2, c, d) * k_order(2, a, b))
        return numerator / (observ_prob_nocov(a, b, c, d, 0, 0))


def a_tilde_independence_nocov(a, b, c, d, z_1, z_2):

    x = exp_theta_independence_nocov(a, b, c, d, z_1, z_2)
    y = exp_square_theta_independence_nocov(a, b, c, d, z_1, z_2)

    return x * (x - y) / (y - x ** 2)


def b_tilde_independence_nocov(a, b, c, d, z_1, z_2):

    x = exp_theta_independence_nocov(a, b, c, d, z_1, z_2)
    y = exp_square_theta_independence_nocov(a, b, c, d, z_1, z_2)

    return (1 - x) * (x - y) / (y - x ** 2)


def c_tilde_independence_nocov(a, b, c, d, z_1, z_2):

    x = exp_rho_independence_nocov(a, b, c, d, z_1, z_2)
    y = exp_square_rho_independence_nocov(a, b, c, d, z_1, z_2)

    return x * (x - y) / (y - x ** 2)


def d_tilde_independence_nocov(a, b, c, d, z_1, z_2):

    x = exp_rho_independence_nocov(a, b, c, d, z_1, z_2)
    y = exp_square_rho_independence_nocov(a, b, c, d, z_1, z_2)

    return (1 - x) * (x - y) / (y - x ** 2)


def R_peer_only_independence_nocov(a, b, c, d, z_1, z_2):

    a_updated = a_tilde_independence_nocov(a, b, c, d, z_1, z_2)
    b_updated = b_tilde_independence_nocov(a, b, c, d, z_1, z_2)
    value     = small_h(big_I(a_updated, b_updated)) -  small_h(big_I(a, b))

    return value
# --------------------------------



# benchmark 1: only single workers are available --------------------------------------------------
def benchmark_single_only(cost_single, budget, K):

    """Get the performance of benchmark 1.

        Args:
            cost_single    : cost of every single workers
            budget         : total budget, in this case, is the time budget
            K              : number of instances

        Returns:
            accuracy
    """

    instance_stat_single_benchmark = [[1, 1] for _ in range(K)]
    rho_stat_single_benchmark      = [4, 1]

    current_budget = 0
    while current_budget < budget:

        R_plus_list = []
        for k in range(K):
            R_plus = max(R_single_benchmark(instance_stat_single_benchmark[k][0], instance_stat_single_benchmark[k][1],
                                            rho_stat_single_benchmark[0], rho_stat_single_benchmark[1], 1),
                         R_single_benchmark(instance_stat_single_benchmark[k][0], instance_stat_single_benchmark[k][1],
                                            rho_stat_single_benchmark[0], rho_stat_single_benchmark[1], 0))
            R_plus_list.append(R_plus)

        selected_instance = R_plus_list.index(max(R_plus_list))

        tmp_instance_theta_stat = copy.deepcopy(instance_stat_single_benchmark)
        tmp_rho_stat_single     = copy.deepcopy(rho_stat_single_benchmark)

        # randomly draw from real data
        draw_item = random.choice(list(set(single_worker_df.index[single_worker_df['image'] == str(selected_instance + 1)].tolist())&
                                      (set(single_worker_df.index[single_worker_df['taskId'] == str(1)].tolist())|
                                       set(single_worker_df.index[single_worker_df['taskId'] == str(2)].tolist()))))
        z = 1 if single_worker_df.iloc[draw_item]['answer'] == 'h' else 0

        instance_stat_single_benchmark[selected_instance][0] = a_tilde(tmp_instance_theta_stat[selected_instance][0],
                                                                       tmp_instance_theta_stat[selected_instance][1],
                                                                       tmp_rho_stat_single[0],
                                                                       tmp_rho_stat_single[1],
                                                                       z)
        instance_stat_single_benchmark[selected_instance][1] = b_tilde(tmp_instance_theta_stat[selected_instance][0],
                                                                       tmp_instance_theta_stat[selected_instance][1],
                                                                       tmp_rho_stat_single[0],
                                                                       tmp_rho_stat_single[1],
                                                                       z)

        rho_stat_single_benchmark[0] = c_tilde(tmp_instance_theta_stat[selected_instance][0],
                                                                  tmp_instance_theta_stat[selected_instance][1],
                                                                  tmp_rho_stat_single[0],
                                                                  tmp_rho_stat_single[1],
                                                                  z)

        rho_stat_single_benchmark[1] = d_tilde(tmp_instance_theta_stat[selected_instance][0],
                                                                  tmp_instance_theta_stat[selected_instance][1],
                                                                  tmp_rho_stat_single[0],
                                                                  tmp_rho_stat_single[1],
                                                                  z)

        current_budget += cost_single

    H_get_positive = []
    H_get_negative = []
    for index in range(K):
        if instance_stat_single_benchmark[index][0] > instance_stat_single_benchmark[index][1]:
            H_get_positive.append(index)
        elif instance_stat_single_benchmark[index][0] == instance_stat_single_benchmark[index][1]:
            if np.random.rand() >= 0.5:
                H_get_positive.append(index)
            else:
                H_get_negative.append(index)
        else:
            H_get_negative.append(index)
    accuracy = (len(set(H_get_positive) & set(H_positive)) + len(set(H_get_negative) & set(H_negative))) / K
    return accuracy
# -------------------------------------------------------------------------------------------------




# benchmark 2: round-robinly choosing the images to label, only single worker are availbel --------
def benchmark_round_robinly_single_only(cost_single, budget, K):

    """Get the performance of benchmark 2.

        Args:
            cost_single    : cost of every single workers
            budget         : total budget, in this case, is the time budget
            K              : number of instances

        Returns:
            accuracy
    """

    instance_stat_single_benchmark = [[0, 0] for _ in range(K)]

    current_budget = 0
    selected_instance = 0
    while current_budget < budget:

        # randomly draw from real data
        draw_item = random.choice(list(set(single_worker_df.index[single_worker_df['image'] == str(selected_instance + 1)].tolist())&
                                      (set(single_worker_df.index[single_worker_df['taskId'] == str(1)].tolist())|
                                       set(single_worker_df.index[single_worker_df['taskId'] == str(2)].tolist()))))
        z = 1 if single_worker_df.iloc[draw_item]['answer'] == 'h' else 0
        if z == 1:
            instance_stat_single_benchmark[selected_instance][0] += 1
        else:
            instance_stat_single_benchmark[selected_instance][1] += 1

        selected_instance += 1
        if selected_instance == 20:
            selected_instance = 0

        current_budget += cost_single

    H_get_positive = []
    H_get_negative = []
    for index in range(K):
        if instance_stat_single_benchmark[index][0] > instance_stat_single_benchmark[index][1]:
            H_get_positive.append(index)
        elif instance_stat_single_benchmark[index][0] == instance_stat_single_benchmark[index][1]:
            if np.random.rand() >= 0.5:
                H_get_positive.append(index)
            else:
                H_get_negative.append(index)
        else:
            H_get_negative.append(index)
    accuracy = (len(set(H_get_positive) & set(H_positive)) + len(set(H_get_negative) & set(H_negative))) / K
    return accuracy
# -------------------------------------------------------------------------------------------------




# benchmark 3: only peer workers available and assume [Independence]  -----------------------------
def benchmark_peer_only_independence(cost_peer, budget, K):

    """Get the performance when only peer workers are available on real data and assume they are INDEPENDENT.

        Args:
            cost_peer     : a list of cost for hiring one pair (two workers)
            budget        : total budget, in this case, is the time budget
            K             : number of instances

        Returns:
            accuracy
    """

    instance_theta_peer_only_stat = [[1, 1] for _ in range(K)]
    rho_peer_only_stat            = [4, 1]

    current_budget = 0
    while current_budget < budget:

        R_plus_list = []
        for k in range(K):
            # peer communication
            # R_plus_peer = max(
            #     R_peer_only_independence_nocov(instance_theta_peer_only_stat[k][0], instance_theta_peer_only_stat[k][1],
            #                                    rho_peer_only_stat[0],               rho_peer_only_stat[1],
            #                                    1, 1),
            #     R_peer_only_independence_nocov(instance_theta_peer_only_stat[k][0], instance_theta_peer_only_stat[k][1],
            #                                    rho_peer_only_stat[0],               rho_peer_only_stat[1],
            #                                    1, 0),
            #     R_peer_only_independence_nocov(instance_theta_peer_only_stat[k][0], instance_theta_peer_only_stat[k][1],
            #                                    rho_peer_only_stat[0],               rho_peer_only_stat[1],
            #                                    0, 1),
            #     R_peer_only_independence_nocov(instance_theta_peer_only_stat[k][0], instance_theta_peer_only_stat[k][1],
            #                                    rho_peer_only_stat[0],               rho_peer_only_stat[1],
            #                                    0, 0))
            R_plus_peer = max(
                R_peer(instance_theta_peer_only_stat[k][0], instance_theta_peer_only_stat[k][1], rho_peer_only_stat[0], rho_peer_only_stat[1],
                       1, 1, lagrange_lambda),
                R_peer(instance_theta_peer_only_stat[k][0], instance_theta_peer_only_stat[k][1], rho_peer_only_stat[0], rho_peer_only_stat[1],
                       0, 1, lagrange_lambda),
                R_peer(instance_theta_peer_only_stat[k][0], instance_theta_peer_only_stat[k][1], rho_peer_only_stat[0], rho_peer_only_stat[1],
                       1, 0, lagrange_lambda),
                R_peer(instance_theta_peer_only_stat[k][0], instance_theta_peer_only_stat[k][1], rho_peer_only_stat[0], rho_peer_only_stat[1],
                       0, 0, lagrange_lambda))


            R_plus_list.append(R_plus_peer)

        selected_instance = R_plus_list.index(max(R_plus_list))
        # print("selected_instance is                     :", selected_instance)

        tmp_instance_theta_peer_only_stat = copy.deepcopy(instance_theta_peer_only_stat)
        tmp_rho__peer_only_stat           = copy.deepcopy(rho_peer_only_stat)

        # randomly draw one pair (two labels) from real data
        selected_instance_list = list(set(peer_worker_df.index[peer_worker_df['image'] == str(selected_instance + 1)].tolist()) &
                                      (set(peer_worker_df.index[peer_worker_df['taskId'] == str(1)].tolist()) |
                                       set(peer_worker_df.index[peer_worker_df['taskId'] == str(2)].tolist())))
        selected_instance_list = sorted(selected_instance_list, key=int)
        peer_seed = random.choice(np.linspace(0, int(len(selected_instance_list) / 2) - 1, int(len(selected_instance_list) / 2)))
        index_1 = selected_instance_list[int(2 * peer_seed)]
        index_2 = selected_instance_list[int(2 * peer_seed + 1)]
        z_1 = 1 if peer_worker_df.iloc[index_1]['answer'] == 'h' else 0
        z_2 = 1 if peer_worker_df.iloc[index_2]['answer'] == 'h' else 0

        # print("obtain labels for instance %d are: (%d, %d)" %(selected_instance, z_1, z_2))
        instance_theta_peer_only_stat[selected_instance][0] = a_tilde_independence_nocov(tmp_instance_theta_peer_only_stat[selected_instance][0],
                                                                                         tmp_instance_theta_peer_only_stat[selected_instance][1],
                                                                                         tmp_rho__peer_only_stat[0],
                                                                                         tmp_rho__peer_only_stat[1],
                                                                                         z_1, z_2)
        instance_theta_peer_only_stat[selected_instance][1] = b_tilde_independence_nocov(tmp_instance_theta_peer_only_stat[selected_instance][0],
                                                                                         tmp_instance_theta_peer_only_stat[selected_instance][1],
                                                                                         tmp_rho__peer_only_stat[0],
                                                                                         tmp_rho__peer_only_stat[1],
                                                                                         z_1, z_2)
        rho_peer_only_stat[0]                               = c_tilde_independence_nocov(tmp_instance_theta_peer_only_stat[selected_instance][0],
                                                                                         tmp_instance_theta_peer_only_stat[selected_instance][1],
                                                                                         tmp_rho__peer_only_stat[0],
                                                                                         tmp_rho__peer_only_stat[1],
                                                                                         z_1, z_2)
        rho_peer_only_stat[1]                               = d_tilde_independence_nocov(tmp_instance_theta_peer_only_stat[selected_instance][0],
                                                                                         tmp_instance_theta_peer_only_stat[selected_instance][1],
                                                                                         tmp_rho__peer_only_stat[0],
                                                                                         tmp_rho__peer_only_stat[1],
                                                                                         z_1, z_2)
        current_budget += cost_peer

    H_get_positive = []
    H_get_negative = []
    for index in range(K):
        if instance_theta_peer_only_stat[index][0] > instance_theta_peer_only_stat[index][1]:
            H_get_positive.append(index)
        elif instance_theta_peer_only_stat[index][0] == instance_theta_peer_only_stat[index][1]:
            if np.random.rand() >= 0.5:
                H_get_positive.append(index)
            else:
                H_get_negative.append(index)
        else:
            H_get_negative.append(index)

    accuracy = (len(set(H_get_positive) & set(H_positive)) + len(set(H_get_negative) & set(H_negative))) / K
    return accuracy
# -------------------------------------------------------------------------------------------------




# Applying CMDP when single workers and peer workers are both availbel ----------------------------
def constrained_MDP(cost_single, cost_peer, lagrange_lambda, budget, K):

    """Get the performance of constrained MDP on real data.

        Args:
            cost_single    : cost of every single workers
            cost_peer      : cost of every peer workers
            budget         : total budget, in this case, is the time budget
            lagrange_lambda: lagrange multiplier
            K              : number of instances

        Returns:
            accuracy
    """

    instance_theta_stat = [[1, 1] for _ in range(K)]
    rho_stat_single     = [2, 1]
    rho_stat_peer       = [4, 1]

    action_taken        = []

    current_budget = 0
    while current_budget < budget:

        R_plus_list = [[0 for _ in range(2)] for _ in range(K)]
        for k in range(K):
            # single worker:
            R_plus_single = max(
                R_single(instance_theta_stat[k][0], instance_theta_stat[k][1], rho_stat_single[0],
                         rho_stat_single[1], 1, lagrange_lambda),
                R_single(instance_theta_stat[k][0], instance_theta_stat[k][1], rho_stat_single[0],
                         rho_stat_single[1], 0, lagrange_lambda))

            # peer communication
            R_plus_peer = max(
                R_peer(instance_theta_stat[k][0], instance_theta_stat[k][1], rho_stat_peer[0], rho_stat_peer[1],
                       1, 1, lagrange_lambda),
                R_peer(instance_theta_stat[k][0], instance_theta_stat[k][1], rho_stat_peer[0], rho_stat_peer[1],
                       0, 1, lagrange_lambda),
                R_peer(instance_theta_stat[k][0], instance_theta_stat[k][1], rho_stat_peer[0], rho_stat_peer[1],
                       1, 0, lagrange_lambda),
                R_peer(instance_theta_stat[k][0], instance_theta_stat[k][1], rho_stat_peer[0], rho_stat_peer[1],
                       0, 0, lagrange_lambda))

            # -------------------------------------------

            R_plus_list[k][0] = R_plus_single
            R_plus_list[k][1] = R_plus_peer

        R_plus_array = np.array(R_plus_list)
        selected_instance = np.where(R_plus_array == R_plus_array.max())[0][0]
        selected_labelling_strategy = np.where(R_plus_array == R_plus_array.max())[1][0]

        action_taken.append(selected_labelling_strategy)

        if selected_labelling_strategy == 0:  # single worker strategy

            tmp_instance_theta_stat = copy.deepcopy(instance_theta_stat)
            tmp_rho_stat_single = copy.deepcopy(rho_stat_single)

            # randomly draw from real data
            draw_item = random.choice(list(set(single_worker_df.index[single_worker_df['image'] == str(selected_instance + 1)].tolist())&
                                          (set(single_worker_df.index[single_worker_df['taskId'] == str(1)].tolist())|
                                           set(single_worker_df.index[single_worker_df['taskId'] == str(2)].tolist()))))
            z = 1 if single_worker_df.iloc[draw_item]['answer'] == 'h' else 0

            instance_theta_stat[selected_instance][0] = a_tilde(tmp_instance_theta_stat[selected_instance][0],
                                                                tmp_instance_theta_stat[selected_instance][1],
                                                                tmp_rho_stat_single[0],
                                                                tmp_rho_stat_single[1],
                                                                z)
            instance_theta_stat[selected_instance][1] = b_tilde(tmp_instance_theta_stat[selected_instance][0],
                                                                tmp_instance_theta_stat[selected_instance][1],
                                                                tmp_rho_stat_single[0],
                                                                tmp_rho_stat_single[1],
                                                                z)

            rho_stat_single[0] = c_tilde(tmp_instance_theta_stat[selected_instance][0],
                                                            tmp_instance_theta_stat[selected_instance][1],
                                                            tmp_rho_stat_single[0],
                                                            tmp_rho_stat_single[1],
                                                            z)

            rho_stat_single[1] = d_tilde(tmp_instance_theta_stat[selected_instance][0],
                                                            tmp_instance_theta_stat[selected_instance][1],
                                                            tmp_rho_stat_single[0],
                                                            tmp_rho_stat_single[1],
                                                            z)

            current_budget += cost_single

        if selected_labelling_strategy == 1:  # peer worker strategy

            tmp_instance_theta_stat_peer = copy.deepcopy(instance_theta_stat)
            tmp_rho_stat_peer = copy.deepcopy(rho_stat_peer)

            # randomly draw from real data
            selected_instance_list = list(set(peer_worker_df.index[peer_worker_df['image'] == str(selected_instance + 1)].tolist())&
                                         (set(peer_worker_df.index[peer_worker_df['taskId'] == str(1)].tolist())|
                                          set(peer_worker_df.index[peer_worker_df['taskId'] == str(2)].tolist())))
            selected_instance_list = sorted(selected_instance_list, key=int)
            peer_seed = random.choice(np.linspace(0, int(len(selected_instance_list) / 2) - 1, int(len(selected_instance_list) / 2)))
            index_1 = selected_instance_list[int(2 * peer_seed)]
            index_2 = selected_instance_list[int(2 * peer_seed + 1)]
            z_1 = 1 if peer_worker_df.iloc[index_1]['answer'] == 'h' else 0
            z_2 = 1 if peer_worker_df.iloc[index_2]['answer'] == 'h' else 0

            # Method 1
            instance_theta_stat[selected_instance][0] = \
            twice_updating(tmp_instance_theta_stat_peer[selected_instance][0],
                           tmp_instance_theta_stat_peer[selected_instance][1],
                           tmp_rho_stat_peer[0],
                           tmp_rho_stat_peer[1],
                           z_1, z_2)[0]
            instance_theta_stat[selected_instance][1] = \
            twice_updating(tmp_instance_theta_stat_peer[selected_instance][0],
                           tmp_instance_theta_stat_peer[selected_instance][1],
                           tmp_rho_stat_peer[0],
                           tmp_rho_stat_peer[1],
                           z_1, z_2)[1]
            rho_stat_peer[0] = twice_updating(tmp_instance_theta_stat_peer[selected_instance][0],
                                                                 tmp_instance_theta_stat_peer[selected_instance][1],
                                                                 tmp_rho_stat_peer[0],
                                                                 tmp_rho_stat_peer[1],
                                                                 z_1, z_2)[2]

            rho_stat_peer[1] = twice_updating(tmp_instance_theta_stat_peer[selected_instance][0],
                                                                 tmp_instance_theta_stat_peer[selected_instance][1],
                                                                 tmp_rho_stat_peer[0],
                                                                 tmp_rho_stat_peer[1],
                                                                 z_1, z_2)[3]

            current_budget += cost_peer

    H_get_positive = []
    H_get_negative = []
    for index in range(K):
        if instance_theta_stat[index][0] > instance_theta_stat[index][1]:
            H_get_positive.append(index)
        elif instance_theta_stat[index][0] == instance_theta_stat[index][1]:
            if np.random.rand() >= 0.5:
                H_get_positive.append(index)
            else:
                H_get_negative.append(index)
        else:
            H_get_negative.append(index)

    accuracy = (len(set(H_get_positive) & set(H_positive)) + len(set(H_get_negative) & set(H_negative))) / K

    return accuracy
# -------------------------------------------------------------------------------------------------



# benchmark 3: only peer workers available and assume [dependence aggregation with MLE]  -----------------------------
def benchmark_peer_only_dependence_MLE(cost_peer, budget, K):

    """Get the performance when only peer workers are available on real data and assume they are INDEPENDENT.

        Args:
            cost_peer     : a list of cost for hiring one pair (two workers)
            budget        : total budget, in this case, is the time budget
            K             : number of instances

        Returns:
            accuracy
    """

    instance_theta_peer_only_stat = [[1, 1] for _ in range(K)]
    rho_peer_only_stat            = [4, 1]

    current_budget = 0
    while current_budget < budget:

        R_plus_list = []
        for k in range(K):
            # peer communication
            R_plus_peer = max(
                R_peer_MLE(instance_theta_peer_only_stat[k][0], instance_theta_peer_only_stat[k][1], rho_peer_only_stat[0], rho_peer_only_stat[1],
                            1, lagrange_lambda, cost_peer),
                R_peer_MLE(instance_theta_peer_only_stat[k][0], instance_theta_peer_only_stat[k][1], rho_peer_only_stat[0], rho_peer_only_stat[1],
                            0, lagrange_lambda, cost_peer))

            R_plus_list.append(R_plus_peer)

        selected_instance = R_plus_list.index(max(R_plus_list))

        tmp_instance_theta_peer_only_stat = copy.deepcopy(instance_theta_peer_only_stat)
        tmp_rho__peer_only_stat           = copy.deepcopy(rho_peer_only_stat)

        # randomly draw one pair (two labels) from real data
        selected_instance_list = list(set(peer_worker_df.index[peer_worker_df['image'] == str(selected_instance + 1)].tolist()) &
                                      (set(peer_worker_df.index[peer_worker_df['taskId'] == str(1)].tolist()) |
                                       set(peer_worker_df.index[peer_worker_df['taskId'] == str(2)].tolist())))
        selected_instance_list = sorted(selected_instance_list, key=int)
        peer_seed = random.choice(np.linspace(0, int(len(selected_instance_list) / 2) - 1, int(len(selected_instance_list) / 2)))
        index_1 = selected_instance_list[int(2 * peer_seed)]
        index_2 = selected_instance_list[int(2 * peer_seed + 1)]
        z_1 = 1 if peer_worker_df.iloc[index_1]['answer'] == 'h' else 0
        z_2 = 1 if peer_worker_df.iloc[index_2]['answer'] == 'h' else 0

        # Method 1
        if z_1 + z_2 == 1:
            current_budget += cost_peer
        elif z_1 == 1 and z_2 == 1:

            instance_theta_peer_only_stat[selected_instance][0] = a_tilde(tmp_instance_theta_peer_only_stat[selected_instance][0],
                                                                          tmp_instance_theta_peer_only_stat[selected_instance][1],
                                                                          tmp_rho__peer_only_stat[0],
                                                                          tmp_rho__peer_only_stat[1],
                                                                1)
            instance_theta_peer_only_stat[selected_instance][1] = b_tilde(tmp_instance_theta_peer_only_stat[selected_instance][0],
                                                                          tmp_instance_theta_peer_only_stat[selected_instance][1],
                                                                          tmp_rho__peer_only_stat[0],
                                                                          tmp_rho__peer_only_stat[1],
                                                                1)

            rho_peer_only_stat[0] = c_tilde(tmp_instance_theta_peer_only_stat[selected_instance][0],
                                            tmp_instance_theta_peer_only_stat[selected_instance][1],
                                            tmp_rho__peer_only_stat[0],
                                            tmp_rho__peer_only_stat[1],
                                       1)

            rho_peer_only_stat[1] = d_tilde(tmp_instance_theta_peer_only_stat[selected_instance][0],
                                            tmp_instance_theta_peer_only_stat[selected_instance][1],
                                            tmp_rho__peer_only_stat[0],
                                            tmp_rho__peer_only_stat[1],
                                       1)
            current_budget += cost_peer
        else:
            instance_theta_peer_only_stat[selected_instance][0] = a_tilde(tmp_instance_theta_peer_only_stat[selected_instance][0],
                                                                          tmp_instance_theta_peer_only_stat[selected_instance][1],
                                                                          tmp_rho__peer_only_stat[0],
                                                                          tmp_rho__peer_only_stat[1],
                                                                0)
            instance_theta_peer_only_stat[selected_instance][1] = b_tilde(tmp_instance_theta_peer_only_stat[selected_instance][0],
                                                                          tmp_instance_theta_peer_only_stat[selected_instance][1],
                                                                          tmp_rho__peer_only_stat[0],
                                                                          tmp_rho__peer_only_stat[1],
                                                                0)

            rho_peer_only_stat[0] = c_tilde(tmp_instance_theta_peer_only_stat[selected_instance][0],
                                            tmp_instance_theta_peer_only_stat[selected_instance][1],
                                            tmp_rho__peer_only_stat[0],
                                            tmp_rho__peer_only_stat[1],
                                       0)

            rho_peer_only_stat[1] = d_tilde(tmp_instance_theta_peer_only_stat[selected_instance][0],
                                            tmp_instance_theta_peer_only_stat[selected_instance][1],
                                            tmp_rho__peer_only_stat[0],
                                            tmp_rho__peer_only_stat[1],
                                       0)
            current_budget += cost_peer


    H_get_positive = []
    H_get_negative = []
    for index in range(K):
        if instance_theta_peer_only_stat[index][0] > instance_theta_peer_only_stat[index][1]:
            H_get_positive.append(index)
        elif instance_theta_peer_only_stat[index][0] == instance_theta_peer_only_stat[index][1]:
            if np.random.rand() >= 0.5:
                H_get_positive.append(index)
            else:
                H_get_negative.append(index)
        else:
            H_get_negative.append(index)

    accuracy = (len(set(H_get_positive) & set(H_positive)) + len(set(H_get_negative) & set(H_negative))) / K
    return accuracy
# -------------------------------------------------------------------------------------------------




# Applying CMDP with meta worker when single workers and peer workers are both availbel ----------------------------
def constrained_MDP_meta_worker(cost_single, cost_peer, lagrange_lambda, budget, K):

    """Get the performance of constrained MDP on real data.

        Args:
            cost_single    : cost of every single workers
            cost_peer      : cost of every peer workers
            budget         : total budget, in this case, is the time budget
            lagrange_lambda: lagrange multiplier
            K              : number of instances

        Returns:
            accuracy
    """

    instance_theta_stat = [[1, 1] for _ in range(K)]
    rho_stat_single     = [2, 1]
    rho_stat_peer       = [4, 1]

    action_taken        = []

    current_budget = 0
    while current_budget < budget:

        R_plus_list = [[0 for _ in range(2)] for _ in range(K)]
        for k in range(K):
            # single worker:
            R_plus_single = max(
                R_single(instance_theta_stat[k][0], instance_theta_stat[k][1], rho_stat_single[0],
                         rho_stat_single[1], 1, lagrange_lambda),
                R_single(instance_theta_stat[k][0], instance_theta_stat[k][1], rho_stat_single[0],
                         rho_stat_single[1], 0, lagrange_lambda))

            R_plus_peer = max(
                R_peer_MLE(instance_theta_stat[k][0], instance_theta_stat[k][1], rho_stat_peer[0], rho_stat_peer[1],
                       1, lagrange_lambda, cost_peer),
                R_peer_MLE(instance_theta_stat[k][0], instance_theta_stat[k][1], rho_stat_peer[0], rho_stat_peer[1],
                       0, lagrange_lambda, cost_peer))

            # -------------------------------------------

            R_plus_list[k][0] = R_plus_single
            R_plus_list[k][1] = R_plus_peer

        R_plus_array = np.array(R_plus_list)
        selected_instance = np.where(R_plus_array == R_plus_array.max())[0][0]
        selected_labelling_strategy = np.where(R_plus_array == R_plus_array.max())[1][0]
        # print("selected instance is %d and strategy is %d" %(selected_instance, selected_labelling_strategy))
        action_taken.append(selected_labelling_strategy)

        if selected_labelling_strategy == 0:  # single worker strategy

            tmp_instance_theta_stat = copy.deepcopy(instance_theta_stat)
            tmp_rho_stat_single = copy.deepcopy(rho_stat_single)

            # randomly draw from real data
            draw_item = random.choice(list(set(single_worker_df.index[single_worker_df['image'] == str(selected_instance + 1)].tolist())&
                                          (set(single_worker_df.index[single_worker_df['taskId'] == str(1)].tolist())|
                                           set(single_worker_df.index[single_worker_df['taskId'] == str(2)].tolist()))))
            z = 1 if single_worker_df.iloc[draw_item]['answer'] == 'h' else 0

            instance_theta_stat[selected_instance][0] = a_tilde(tmp_instance_theta_stat[selected_instance][0],
                                                                tmp_instance_theta_stat[selected_instance][1],
                                                                tmp_rho_stat_single[0],
                                                                tmp_rho_stat_single[1],
                                                                z)
            instance_theta_stat[selected_instance][1] = b_tilde(tmp_instance_theta_stat[selected_instance][0],
                                                                tmp_instance_theta_stat[selected_instance][1],
                                                                tmp_rho_stat_single[0],
                                                                tmp_rho_stat_single[1],
                                                                z)

            rho_stat_single[0] = c_tilde(tmp_instance_theta_stat[selected_instance][0],
                                                            tmp_instance_theta_stat[selected_instance][1],
                                                            tmp_rho_stat_single[0],
                                                            tmp_rho_stat_single[1],
                                                            z)

            rho_stat_single[1] = d_tilde(tmp_instance_theta_stat[selected_instance][0],
                                                            tmp_instance_theta_stat[selected_instance][1],
                                                            tmp_rho_stat_single[0],
                                                            tmp_rho_stat_single[1],
                                                            z)

            current_budget += cost_single

        if selected_labelling_strategy == 1:  # peer worker strategy

            tmp_instance_theta_stat_peer = copy.deepcopy(instance_theta_stat)
            tmp_rho_stat_peer = copy.deepcopy(rho_stat_peer)

            # randomly draw from real data
            selected_instance_list = list(set(peer_worker_df.index[peer_worker_df['image'] == str(selected_instance + 1)].tolist())&
                                         (set(peer_worker_df.index[peer_worker_df['taskId'] == str(1)].tolist())|
                                          set(peer_worker_df.index[peer_worker_df['taskId'] == str(2)].tolist())))
            selected_instance_list = sorted(selected_instance_list, key=int)
            peer_seed = random.choice(np.linspace(0, int(len(selected_instance_list) / 2) - 1, int(len(selected_instance_list) / 2)))
            index_1 = selected_instance_list[int(2 * peer_seed)]
            index_2 = selected_instance_list[int(2 * peer_seed + 1)]
            z_1 = 1 if peer_worker_df.iloc[index_1]['answer'] == 'h' else 0
            z_2 = 1 if peer_worker_df.iloc[index_2]['answer'] == 'h' else 0

            # Method 1
            if   z_1 + z_2 == 1:
                current_budget += cost_peer
            elif z_1 == 1 and z_2 == 1:

                instance_theta_stat[selected_instance][0] = a_tilde(tmp_instance_theta_stat_peer[selected_instance][0],
                                                                    tmp_instance_theta_stat_peer[selected_instance][1],
                                                                    tmp_rho_stat_peer[0],
                                                                    tmp_rho_stat_peer[1],
                                                                    1)
                instance_theta_stat[selected_instance][1] = b_tilde(tmp_instance_theta_stat_peer[selected_instance][0],
                                                                    tmp_instance_theta_stat_peer[selected_instance][1],
                                                                    tmp_rho_stat_peer[0],
                                                                    tmp_rho_stat_peer[1],
                                                                    1)

                rho_stat_peer[0] = c_tilde(tmp_instance_theta_stat_peer[selected_instance][0],
                                           tmp_instance_theta_stat_peer[selected_instance][1],
                                           tmp_rho_stat_peer[0],
                                           tmp_rho_stat_peer[1],
                                           1)

                rho_stat_peer[1] = d_tilde(tmp_instance_theta_stat_peer[selected_instance][0],
                                           tmp_instance_theta_stat_peer[selected_instance][1],
                                           tmp_rho_stat_peer[0],
                                           tmp_rho_stat_peer[1],
                                           1)
                current_budget += cost_peer
            else:
                instance_theta_stat[selected_instance][0] = a_tilde(tmp_instance_theta_stat_peer[selected_instance][0],
                                                                    tmp_instance_theta_stat_peer[selected_instance][1],
                                                                    tmp_rho_stat_peer[0],
                                                                    tmp_rho_stat_peer[1],
                                                                    0)
                instance_theta_stat[selected_instance][1] = b_tilde(tmp_instance_theta_stat_peer[selected_instance][0],
                                                                    tmp_instance_theta_stat_peer[selected_instance][1],
                                                                    tmp_rho_stat_peer[0],
                                                                    tmp_rho_stat_peer[1],
                                                                    0)

                rho_stat_peer[0] = c_tilde(tmp_instance_theta_stat_peer[selected_instance][0],
                                           tmp_instance_theta_stat_peer[selected_instance][1],
                                           tmp_rho_stat_peer[0],
                                           tmp_rho_stat_peer[1],
                                           0)

                rho_stat_peer[1] = d_tilde(tmp_instance_theta_stat_peer[selected_instance][0],
                                           tmp_instance_theta_stat_peer[selected_instance][1],
                                           tmp_rho_stat_peer[0],
                                           tmp_rho_stat_peer[1],
                                           0)
                current_budget += cost_peer


    H_get_positive = []
    H_get_negative = []
    for index in range(K):
        if instance_theta_stat[index][0] > instance_theta_stat[index][1]:
            H_get_positive.append(index)
        elif instance_theta_stat[index][0] == instance_theta_stat[index][1]:
            if np.random.rand() >= 0.5:
                H_get_positive.append(index)
            else:
                H_get_negative.append(index)
        else:
            H_get_negative.append(index)

    accuracy = (len(set(H_get_positive) & set(H_positive)) + len(set(H_get_negative) & set(H_negative))) / K

    return accuracy
# -------------------------------------------------------------------------------------------------




# -----------------------------------------------------------
if __name__=="__main__":

    # readin the real data
    with open('answer.csv', 'r') as csvfile:
        single_worker = csv.reader(csvfile, delimiter=',')
        single_worker_list = list(single_worker)

    single_worker_headers = single_worker_list.pop(0)
    single_worker_df = pd.DataFrame(single_worker_list, columns = single_worker_headers)

    with open('o_peer_communication.csv', 'r') as csvfile:
        peer_worker = csv.reader(csvfile, delimiter=',')
        peer_worker_list = list(peer_worker)

    peer_worker_headers = peer_worker_list.pop(0)
    peer_worker_df = pd.DataFrame(peer_worker_list, columns=peer_worker_headers)

    # ------------------------
    parser = argparse.ArgumentParser(description='CMDP Parameters.')

    parser.add_argument("--number-of-experiments",  "-N",  required=True, type=int, help="Number of Experiments")
    parser.add_argument("--number-of-instances",    "-K",  required=True, type=int, help="Number of Instances")
    parser.add_argument("--budget-list",            "-b",  nargs='+',     required=True,  type=int, help="Budget List")
    parser.add_argument("--cost-single",            "-c-s",type = float, default=1,
                        help="Cost of Single Workers: defoult value is 1",)
    parser.add_argument("--cost-peer-list",         "-c-p",nargs='+', required=True, type = float, help="Cost List of Peer Workers",)
    parser.add_argument("--lagrange-lambda",        "-lambda",  required=True, type = float, help="Lagrange Lambda Value",)

    args = parser.parse_args()

    # positive set and negative set:
    H_positive = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    H_negative = [10, 11, 12, 13, 14, 15, 16, 17, 18, 19]

    # number of experiments
    N                = args.number_of_experiments

    # number of instances
    K                = args.number_of_instances

    # budget
    budget_list      = args.budget_list

    # cost for actions:
    cost_single      = args.cost_single
    cost_peer_list   = args.cost_peer_list

    # Lagrangian multiplier
    lagrange_lambda  = args.lagrange_lambda


    print("-----------------------------------Parameters Start")
    print("number of experiments  :", N)
    print("number of instances    :", K)
    print("budget list            :", budget_list)
    print("cost for single workers:", cost_single)
    print("cost for peer workers  :", cost_peer_list)
    print("lambda is              :", lagrange_lambda)
    print("-----------------------------------Parameters End")


    # -----------------------------------------------------------
    # get the results from benchamrk 1 -- single workers only
    acc_single_only_list = []
    for exp in range(1, N+1):

        acc_single_only = []
        for budget in budget_list:
            acc_single_only.append(benchmark_single_only( cost_single, budget, K))

        acc_single_only_list.append(acc_single_only)
        print("benchmark 1 (single only) exp ---- %d is done." % exp)

    print("detailed acc benchmark 1 (single only) are:", acc_single_only_list)
    avg_acc_single_only = []
    for i in range(len(budget_list)):
        avg_acc_single_only.append(sum([acc_single_only_list[j][i] for j in range(N)])/N)
    print("avg_acc_single_only are:", avg_acc_single_only)
    # -----------------------------------------------------------



    """
    # -----------------------------------------------------------
    # get the results from benchamrk 2 -- round_robinly strategy
    acc_round_robinly_list = []
    for exp in range(1, N + 1):

        acc_round_robinly = []
        for budget in budget_list:
            acc_round_robinly.append(benchmark_round_robinly_single_only(cost_single, budget, K))

        acc_round_robinly_list.append(acc_round_robinly)
        print("benchmark 2 (round robinly, single only) exp ---- %d is done." % exp)

    print("detailed acc benchmark 2 (round robinly, single only) are:", acc_round_robinly_list)
    avg_acc_round_robinly = []
    for i in range(len(budget_list)):
        avg_acc_round_robinly.append(sum([acc_round_robinly_list[j][i] for j in range(N)]) / N)
    print("avg_acc_round_robinly are:", avg_acc_round_robinly)
    # -----------------------------------------------------------
    """


    """
    # -----------------------------------------------------------
    # get the results from benchmark 3 -- only peer workers are availble and assume they are INDEPENDENCE
    acc_peer_only_independence_list = []
    for exp in range(1, N + 1):
        acc_peer_only_independence = []
        for cost_peer in [2.0]:
            accuracy = []
            for budget in budget_list:
                accuracy.append(benchmark_peer_only_independence(cost_peer, budget, K))
                print("budget -- %d is done." % budget)
            acc_peer_only_independence.append(accuracy)
            print("c_p -- %.2f is done." % cost_peer)
        acc_peer_only_independence_list.append(acc_peer_only_independence)
        print("benchmark 3 (peer only) [InDependence] exp ---- %d is done." % exp)

    print("detailed acc benchmark 3 (peer only) [Independence] are:", acc_peer_only_independence_list)
    avg_acc_peer_only_independence = []
    for i in range(len([2.0])):
        avg_acc_peer_only_independence.append([sum([acc_peer_only_independence_list[k][i][j] for k in range(N)]) / N for j in range(len(budget_list))])
    print("avg_acc_peer_only_independence [Independence] are:", avg_acc_peer_only_independence)
    # -----------------------------------------------------------
    """


    """
    # -----------------------------------------------------------
    # get the results from CMDP: Xi Chen
    acc_CMDP_list = []
    for exp in range(1, N+1):

        acc_CMDP = []
        for cost_peer in cost_peer_list:
            acc = []
            for budget in budget_list:
                acc.append(constrained_MDP(cost_single, cost_peer, lagrange_lambda, budget, K))

            acc_CMDP.append(acc)
            print("c_p -- %.2f is done." % cost_peer)
        acc_CMDP_list.append(acc_CMDP)
        print("CMDP exp ---- %d is done." % exp)

    print("detailed acc CMDP are:", acc_CMDP_list)
    avg_acc_CMDP = []
    for i in range(len(cost_peer_list)):
        avg_acc_CMDP.append([sum([acc_CMDP_list[k][i][j] for k in range(N)])/N for j in range(len(budget_list))])
    print("avg_acc_CMDP [Xi Chen] are:", avg_acc_CMDP)
    # -----------------------------------------------------------
    """

    """
    # Meta Worker aggregation
    # -----------------------------------------------------------
    # get the results from Constrained MDP with meta worker
    acc_CMDP_list = []
    for exp in range(1, N+1):

        acc_CMDP = []
        for cost_peer in cost_peer_list:
            acc = []
            for budget in budget_list:
                acc.append(constrained_MDP_meta_worker(cost_single, cost_peer, lagrange_lambda, budget, K))

            acc_CMDP.append(acc)
            print("c_p -- %.2f is done." % cost_peer)
        acc_CMDP_list.append(acc_CMDP)
        print("CMDP exp ---- %d is done." % exp)

    print("detailed acc CMDP are:", acc_CMDP_list)
    avg_acc_CMDP = []
    for i in range(len(cost_peer_list)):
        avg_acc_CMDP.append([sum([acc_CMDP_list[k][i][j] for k in range(N)])/N for j in range(len(budget_list))])
    print("avg_acc_CMDP are [meta worker]:", avg_acc_CMDP)
    # -----------------------------------------------------------
    """


