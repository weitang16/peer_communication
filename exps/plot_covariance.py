
import matplotlib.pyplot as plt
import numpy as np


# covariance for {0,1} using \theta = Pr(correct answers)
peer_covarince   = [0.0, 0.0667, 0.0625, 0.0556, 0.0769, 0.05, 0.0, 0.0714, 0.8, 0.0, 0.4848, 0.0, 0.15, 0.0476, 0.2353, 0.5185, 0.2667, 0.0435, 0.25, 0.0]
single_covarince = [-0.025, -0.0466, -0.0273, 0.0061, 0.0032, 0.0074, -0.0256, -0.02, 0.4867, -0.0288, 0.0933, -0.0111, -0.0167, -0.0194, -0.0238, 0.1964, 0.0136, 0.0395, -0.001, -0.0022]

# covariance for {0,1} using \theta = Pr(correct answers) for task 3 and task 4; [treatment 1] and [treatment 2]
single_covarince_tm_1 = [0.0, -0.0312, -0.1467, -0.0473, 0.0248, 0.0938, -0.04, -0.0473, 0.4688, 0.0, 0.125, 0.0059, -0.0417, -0.0816, -0.1111, 0.0661, 0.0432, 0.06, -0.037, 0.0]
single_covarince_tm_2 = [0.0, -0.0496, 0.1224,  0.0, 0.0988, 0.0, -0.02, -0.0312, 0.5938, 0.0, 0.0, 0.0, 0.0625, 0.0, -0.02, 0.26, 0.3554, 0.0, 0.125, 0.0]
avg = [(single_covarince_tm_1[i]+single_covarince_tm_2[i])/2 for i in range(len(single_covarince_tm_1))]


# covariance for {1, 0}
# peer_covarince   = [-0.0062, 0.0533, 0.0469, 0.0525, 0.0414, 0.04, 0.0, 0.0663, -0.0089, 0.0, 0.2039, 0.0, 0.1, 0.034, 0.1107, 0.1674, 0.1778, 0.0378, 0.1224, 0.0]
# single_covarince = [-0.0073, -0.0014, 0.0101, 0.0114, 0.016, 0.0185, -0.0065, 0.0075, -0.0123, -0.0144, 0.0334, 0.0067, 0.0083, -0.004, 0.0089, 0.0336, 0.0256, 0.0352, 0.0276, -0.0011]

# # covariance for {+1, -1}
# peer_covarince   = [-0.0247, 0.2133, 0.1875, 0.2099, 0.1657, 0.16, 0.0, 0.2653, -0.0356, 0.0, 0.8154, 0.0, 0.4, 0.1361, 0.4429, 0.6694, 0.7111, 0.1512, 0.4898, 0.0]
# single_covarince = [-0.029, -0.0055, 0.0402, 0.0456, 0.064, 0.074, -0.0262, 0.03, -0.049, -0.0577, 0.1338, 0.0267, 0.0333, -0.016, 0.0357, 0.1344, 0.1025, 0.141, 0.1103, -0.0044]



# f = plt.figure()
# plt.scatter(range(1,21), peer_covarince, label = 'data collected in discussion tasks', marker = "v", color="red")
# plt.scatter(range(1,21), single_covarince, label = 'data collected in independent tasks', marker = "+", color="blue")
#
# plt.xticks([4, 8, 12, 16, 20], [4, 8, 12, 16, 20])
# plt.yticks([-0.1, 0, 0.3, 0.6, 0.9])
# plt.ylabel("Covariance")
# plt.xlabel('Instance index')
# plt.legend(loc = 'upper left', prop={'size': 14})
# f.savefig("covariance.pdf", bbox_inches='tight')


f = plt.figure()
# plt.scatter(range(1,21), single_covarince_tm_1, label = 'independent tasks: task 3&4, treatment 1', marker = "v", color="red")
# plt.scatter(range(1,21), single_covarince_tm_2, label = 'independent tasks: task 3&4, treatment 2', marker = "+", color="blue")

plt.scatter(range(1,21), avg, label = 'independent: task 3&4, treatment 1&2', marker = "+", color="blue")


plt.xticks([4, 8, 12, 16, 20], [4, 8, 12, 16, 20])
plt.yticks([-0.1, 0, 0.3, 0.6, 0.9])
plt.ylabel("Covariance")
plt.xlabel('Instance index')
plt.legend(loc = 'upper left', prop={'size': 14})
f.savefig("covariance_task3&4_treat1&2.pdf", bbox_inches='tight')


