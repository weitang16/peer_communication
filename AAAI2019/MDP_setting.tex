We now turn to our second question on better utilizing \peer in crowdsourcing. In particular, while our experimental study clearly shows that introducing peer communication in crowdwork leads to significant improvement in work quality,
such improvement also comes with extra cost, such as the financial payment incurred to recruit more workers (e.g., at least two workers are needed to form a pair and work
together for peer communication to happen) and the additional administrative cost for synchronizing the work pace of worker pairs. 
As a result, in practice, a requester faces the quality-cost tradeoff, and he needs to strategically decide {\em whether} and {\em when} to use \peer for each task in his batch.
%even before determining how to aggregate the data that is possibly produced from tasks with \peer.
Inspired by the method discussed by~\citet{chen2013optimistic} to optimally allocate budget among task instances in crowdsourcing data collection, we propose an algorithmic approach to help requesters dynamically deploy tasks with peer communication in an optimal way by modeling the decision-making problem as a Bayesian Markov decision process (MDP). %\wt{would that be better if saying "Bayesian Constrained Markov Decision Process (MDP)", agian CMDP consistency}. 
However, compared with the work by \citet{chen2013optimistic}, there are additional challenges in our setting since deploying \peer or not incurs different cost and produces different data.
Therefore, we have formulated our problem as a \emph{constrained} MDP to address these challenges.
%that given the $K$ task instances he has at hand,  
%how should he sequentially recruit workers such that he can obtain the highest overall work quality across all $K$ tasks within a budget of $\mathcal{B}$.
%In particular, the requester seeks for an optimal policy to decide at each time step: (1) which task should he recruit workers to work on, and (2) which recruiting strategy
%should he adopt---recruit one worker to complete the task independently, or two workers to work on the task together with peer communication?

%Inspired by the method discussed in~\cite{chen2013optimistic} to optimally allocate budget among task instances in the crowdsourced data collection process, in this paper,
%we propose an algorithmic approach to help requesters optimally utilize peer communication by modeling the decision-making problem as a {\em constrained} Bayesian Markov Decision Process (MDP).
%For simplicity, we describe our approach below in the context of binary classification tasks (e.g., image labeling tasks that we used in our experimental study). However, our approach can be generalized to the scenario when the number of possible answers for the task is large, and we briefly discuss such generalization later in this section.

\subsubsection{Problem Setup.}

Suppose a requester gets a budget of $\mathcal{B}$ and a batch of $K$ binary classification tasks, and he needs to estimate the label for each of these tasks. 
The goal of the requester is to maximize the average accuracy of the estimated labels across all tasks through spending the budget to solicit labels from crowd workers and then aggregating the collected labels.
Assume the $K$ tasks are independent from each other, and 
$Z_k\in\{0, 1\}$ represents the true label for task $k$ ($1\le k\le K$). We characterize 
the difficulty of task $k$ using a parameter $\theta_k \in [0, 1]$, which is defined as the probability for a ``reliable'' worker to submit a label of $1$ in task $k$. 
We further assume that $\theta_k$ is consistent with $Z_k$, that is, $Z_k=1$ if and only if $\theta_k \geq 0.5.$\footnote{In other words, $\theta_k$ is the {\em soft label} for task $k$, and a worker is reliable if she always randomly samples her submitted hard label in a task based on the soft label.} 
Intuitively, task $k$ is relatively difficult when $\theta_k$ is close to $0.5$. 
  

The requester recruits workers to label his tasks in a sequential manner. Specifically, at each time step $t$, the requester decides on a task $k_t$ to work on, and he can solicit label(s)
from crowd workers on this task using one of the two strategies (the strategy is denoted as $x_t$): 
first, the requester can recruit a {\em single} worker to work on the task ($x_t=0$), and thus obtain {\em one} label for that task; second, the requester
may recruit a {\em pair} of workers to work on the task together following the peer communication procedure ($x_t=1$), and thus obtain {\em two} labels for the task.
Following the discussion in the previous section, below we denote a \emph{\metaworker} as a pair of workers recruited with \peer and a \emph{\metalabel} as the two labels obtained from a meta-worker,


These two recruiting strategies have a few key differences.
First, recruiting a single worker incurs a cost of $c_s$, while recruiting a \metaworker incurs a cost of $c_p$ ($c_p>c_s$).
In terms of the accuracy of the labels, 
%(2) the accuracy of a single worker's independent label is different from the accuracy of the labels generated by a pair of workers after communicating with each
%other. In particular, 
we use $\alpha_s\in[0, 1]$ to represent the skill level of a worker when she works independently, and it is defined as the probability that a worker's independent answer agrees with the answer from a reliable worker. 
Thus, when $x_t=0$, following the notations for collected labels in the previous section, we have:
\begin{align*} 
p_{k_t, s,1}&=\mathbb{P}(y_{t} =s_1|\theta_{k_t}, \alpha_s) = \alpha_{s} \theta_{k_t} + (1-\alpha_{s})(1-\theta_{k_t})\\
p_{k_t, s,0}&=\mathbb{P}(y_{t} =s_0|\theta_{k_t}, \alpha_s) =1-p_{k_t,s,1}
\end{align*}
where $y_{t}$ is the label the requester gets from an independent worker at time step $t$. Intuitively, a higher value of $\alpha_s$ implies a higher accuracy. 
In addition, we use $p_{k_t, 0}\in[0,1]$ and $\alpha_p\in[0, 1]$ to characterize the skill level of a \metaworker. %\wt{still kind of confusing on the intuition of $p_0$} %of a {\em pair} of workers when they work together in a task with peer communication,
Specifically, $p_{k_t, 0}$ is the probability of a \metaworker generating a meta-label of $s_{01}$ in task $k_t$ which, recall from the last section, will effectively be ``discarded'' when the requester aggregates the data. 
Conditioned on a \metaworker contributing a meta-label other than $s_{01}$, 
$\alpha_p$ is similarly defined as $\alpha_s$. 
%and it indicates how often the \metaworker's final answers {\em both} agree with a reliable worker's answer. We further denote the probability for a pair of workers disagreeing with each other in their final answers for task $k_t$ after the discussion as $q_{k_t}$. 
Formally, when $x_t=1$, we have: 
\begin{align*} 
q_{k_t} &= \mathbb{P}(y_{t} = s_{01}|\theta_{k_t}, \alpha_s) = p_{k_t, 0}\\
p_{k_t, p,1}&=\mathbb{P}(y_{t} = s_{11}|\theta_{k_t}, \alpha_s) \\
    &= (1-p_{k_t, 0})( \alpha_{p} \theta_{k_t} + (1-\alpha_{p})(1-\theta_{k_t}))\\
p_{k_t, p, 0} &= \mathbb{P}(y_{t} = s_{00}|\theta_{k_t}, \alpha_s)=1-p_{k_t, p,1}-q_{k_t}
\end{align*}
%where $y_{t}$ is the label the requester gets from one crowd worker at time step $t$. Notice that when $x_t=1$, the requester will get two of such labels in that step. 
Naturally, the requester's activity in each time step can be summarized through the tuple $(k_t, x_t, y_t)$. By
the time $t_{\mathcal{B}}$ that the requester exhausts his budget, his
activity history is $\mathcal{H}_{\mathcal{B}}=\{(k_0, x_0, y_0),...,(k_{t_{\mathcal{B}}}, x_{t_{\mathcal{B}}}, y_{t_{\mathcal{B}}})\}$. The requester then aggregates the data he has collected and infers the true labels for each of the $K$ tasks such that the expected accuracy
across all $K$ tasks conditioned on the activity history $\mathcal{H}_{\mathcal{B}}$ is maximized. 
In other words, 
the requester determines a set of tasks $S_{\mathcal{B}}$ with the inferred label being $1$ by solving the optimization problem: 
\begin{align*}%\label{utility_function}
S_{\mathcal{B}} = \argmax_{S \subset \{1,...,K\}} \mathbb{E}(\sum_{i\in S}\mathbf{1}(Z_i=1) + \sum_{i\notin S}\mathbf{1}(Z_i=0)|\mathcal{H}_{\mathcal{B}})
\end{align*}
where $\mathbf{1}(.)$ is the indicator function.

%\vspace{-8pt}
%\paragraph{A Bayesian Setting.} As the true values of task difficulty (i.e., $\theta_k$) and workers' skill levels (i.e., $\alpha_s$ and $\alpha_p$) are unknown to the requester, we further adopt a Bayesian setting which enables the requesters to learn these values over time. In particular, we assume that the values of $\theta_k$ and $\{\alpha_j\}_{j = s, p}$ are drawn from known Beta prior distributions:
%\begin{align*}
%\theta_k^0 \sim \text{Beta}(a_k^0, b_k^0), \alpha_{j}^0 \sim \text{Beta}(c_{j}^0, d_{j}^0).
%\end{align*}
%
%At time $t$, the requester decides to follow the strategy $x_t$ to recruit worker(s) working on task $k_t$. When $x_t=0$,  the requester will obtain a single label $y_t$ from one worker working independently on task $k_t$, and he can update the posterior distribution of the parameters as follows:
%\begingroup
%\makeatletter\def\f@size{8}
%\begin{align*}
%p(\theta_{k_t}^{t+1}, \alpha_{s}^{t+1}|y_{t}, x_t = 0) = \frac{\mathbb{P}(y_{t}|\theta_{k_t}, \alpha_s, x_t = 0)p(\theta_{k_t}^t)p(\alpha_s^t)} {\mathbb{P}(y_{t}|x_t = 0)} 
%\end{align*}
%\endgroup
%where $\mathbb{P}(y_{t}|\theta_{k_t}, \alpha_s, x_t = 0)$ can be computed using Equation~\ref{pr:single_ability}. When $t=0$, $p(\theta_{k_t}^t)$ and $p(\alpha_s^t)$ refer to the
%probability density functions of the two prior Beta distributions, $\text{Beta}(a_{k_t}^0, b_{k_t}^0)$ and $\text{Beta}(c_{s}^0, d_{s}^0)$. Notice that the product of two Beta priors
%is not a conjugate prior for our likelihood function. Thus, similar to the method used in~\cite{chen2013optimistic}, we approximate the joint posterior distribution by assuming the 
%conditional independence of $\theta_{k_t}$ and $\alpha_s$, and we further use two Beta distributions to approximate the posterior of each parameter. Specifically,
% \begingroup
%\makeatletter\def\f@size{8}
%\begin{align*}
%p(\theta_{k_t}^{t+1}, \alpha_{s}^{t+1}|y_{t}, x_t = 0) &\approx p(\theta_{k_t}^{t+1}|y_{t}, x_t = 0)p(\alpha_{s}^{t+1}|y_{t}, x_t = 0)\\
%\theta_{k_t}^{t+1} &\sim \text{Beta}(a_{k_t}^{t+1}, b_{k_t}^{t+1})\\
%\alpha_{s}^{t+1} &\sim \text{Beta}(c_s^{t+1}, d_s^{t+1})
%\end{align*}
%\endgroup
%where the hyperparameters $a_{k_t}^{t+1}, b_{k_t}^{t+1}, c_s^{t+1}, d_s^{t+1}$ are approximated through moment matching. 
%
%Similarly, when $x_t=1$, the requester will obtain two labels $y_t^{'}, y_t^{''}$ from two workers working together on task $k_t$ with peer communication,
%and he can update his posterior belief on parameter values as follows:
%\begingroup
%\makeatletter\def\f@size{8}
%\begin{align*}
%p(\theta_{k_t}^{t+1}, \alpha_{p}^{t+1}|y_{t}^{'}, y_{t}^{''}, x_t = 1) = \frac{\mathbb{P}(y_{t}^{'}, y_{t}^{''}|\theta_{k_t}, \alpha_p, x_t = 1)p(\theta_{k_t}^t)p(\alpha_p^t)} {\mathbb{P}(y_{t}^{'}, y_{t}^{''}|x_t = 1)} 
%\end{align*}
%\endgroup
%
%We then approximate the joint posterior belief by assuming independence of the two labels, i.e., $\mathbb{P}(y_{t}^{'}, y_{t}^{''}|\theta_{k_t}, \alpha_p, x_t = 1)=\mathbb{P}(y_t^{'}|\theta_{k_t}, \alpha_p, x_t=0)\mathbb{P}(y_t^{''}|\theta_{k_t}, \alpha_p, x_t=1)$\footnote{\my{Say something about what will happen if we consider covariance here.}}, as well as the conditional independence of $\theta_{k_t}$ and $\alpha_p$. Again, we use two Beta distributions
%to approximate the posteriors of these two parameters separately, i.e., $\theta_{k_t}^{t+1} \sim \text{Beta}(a_{k_t}^{t+1}, b_{k_t}^{t+1})$ and $\alpha_{p}^{t+1} \sim \text{Beta}(c_p^{t+1}, d_p^{t+1})$, and the hyperparameters $a_{k_t}^{t+1}, b_{k_t}^{t+1}, c_p^{t+1}, d_p^{t+1}$ are decided through moment matching (See \nameref{appendix: posterior_approx} for more details).
%
%Under such Bayesian setting, by the time  the budget $\mathcal{B}$ is exhausted, the requester has updated his belief on the task difficulty parameters as $\theta_{k}^{t_{\mathcal{B}}+1} \sim \text{Beta}(a_{k}^{t_{\mathcal{B}}+1}, b_{k}^{t_{\mathcal{B}}+1})$. Previous study shows that the optimal solution for Equation~\ref{utility_function} is to assign a label of $1$ to task $k$ if $P_k^{t_{\mathcal{B}}+1}=\mathbb{P}(\theta_k^{t_{\mathcal{B}}+1}\ge0.5)>0.5$, that is, if $a_{k}^{t_{\mathcal{B}}+1}>b_{k}^{t_{\mathcal{B}}+1}$~\cite{xie2013sequential}. %and $\alpha_{j}^{t_{\mathcal{B}}+1} \sim \text{Beta}(c_j^{t_{\mathcal{B}}+1}, d_j^{t_{\mathcal{B}}+1}), j\in\{s, p\}$.
%Following such inference method, the RHS of Equation~\ref{utility_function} can be rewritten as 
%$\sum_{k=1}^K h(P_k^{t_{\mathcal{B}}+1})$ where $h(x) = max(x, 1-x)$. Therefore, the requester essentially needs to solve the following constrained optimization problem:
%\begin{align*} %\label{obj:objective_function}
%\max_{\pi}  & \quad \mathbb{E}^{\pi}(\sum_{k=1}^K h(P_k^{t_{\mathcal{B}}+1})) \\
%\text{s.t.}  & \quad c_s t_s + c_p(t_{\mathcal{B}}+1-t_s) \leq \mathcal{B} 
%\end{align*}
%where $\pi=\{(k_0, x_0),...,(k_{t_{\mathcal{B}}}, x_{t_{\mathcal{B}}})\}$ is the sequence of decisions the requester makes, the expectation $\mathbb{E}^{\pi}$ is taken over all
%the possible activity history $\mathcal{H}_{\mathcal{B}}$ the requester may observe following $\pi$, and $t_s=\sum_{t=0}^{t_{\mathcal{B}}}\mathbf{1}(x_t=0)$ refers to the number of time steps where the requester 
%decides to recruit a single worker to work on the corresponding task independently. Importantly, $t_{\mathcal{B}}$ is a variable in this optimization problem and it may vary with different $\pi$.
%
%\ignore{
%
%The algorithmic approach we propose can be modeled as a constrained Markov Decision Process.
%The requester's decision control problem for an binary image labelling is defined as follows. 
%The requester has multiple images which he wants to know the true labels of these images, and some workers are sequentially arriving, they are asked to return an lable of this image (e.g., whether the dog in the image is a Siberian Husky or a Malamute).
%At each time step, the requester can hire one worker or hire two workers and allow them communicate with each other to complete the task, thus at each time step, the requester needs to make decision on:
%\begin{itemize}
%	\item which image to label?
%	\item which labelling strategy he should adopt, i.e., hiring one worker to work independently, or hiring two workers to work with peer communication? 
%\end{itemize}
%Suppose that there are $K$ instances (images remain to be labelled).
%Each image is a binary labelling problem and they are independent of each other, i.e., each one is independently associated with a true label $Z_k \in \{0, 1\}$  for $k \in [K]$.
%We introduce a parameter $\theta_k \in [0, 1]$ to represent the "baseline labelling difficulty" for image $k$, i.e., the relative frequency that label "1" appears when the number of independent "best" workers approaches infinity.
%As we can see, if $\theta_k$ is close to 0.5, it implies that the $k$-th instance is difficult, because even when the requester asks a large number of "best" workers, there is still no predominant answer. 
%On the other hand, if $\theta_k$  is close to 1 (or 0), then the instance is relatively easy. \wt{still kind of vague on how to define $\theta_k$}
%
%As mentioned above, the requester has two strategies to query labels at each time, one is obtaining label from the single worker, another is from peer workers which are allowed to communicate with each other.
%Define a random variable $x_t \in \{0, 1\}$ to indicate whether the label acquired in this time round $t$ is from the single worker $(x_t  = 0)$ or from the peer communication workers $(x_t  = 1)$.
%One could also view that the labels obtained from these two strategies are equivalently to be obtained from two group workers with different working abilities.
%According to our empricial study, workers with peer communication have a higher correct accuracy on submitting labels, while single workers have a relatively lower correct accuracy.
%We use $\alpha_{s}, \alpha_{p} \in [0,1]$ to capture the different working abilities of single workers and peer communication workers.
%We further adopt \textit{one-coin} model to explain the correct accuracy of these two different worker groups.
%
%
%\subsection{Single Worker's Ability:}
%We firstly assume all single workers in the queue are all homogenous and independent.
%At each time round $t$, the requester chooses an image $I_t \in \{1,...,K\}$ to query its label from a single worker.
%Let $y_{I_t} \in \{0,1\}$ be the obtained label.
%Thus, we have:
%\begin{align} \label{pr:single_ability}
%\mathbb{P}(y_{I_t} = 1|\theta_{I_t}, \alpha_s, x_t = 0) = \alpha_{s} \theta_{I_t} + (1-\alpha_{s})(1-\theta_{I_t})
%\end{align}
%
%\subsection{Peer Communication Worker's Ability:} 
%Note that the requester can obtain two labels if he hires two workers and allows them to communicate to submit their labels in a single round.
%We use $y_{I_t} = \{y_{I_t}^0, y_{I_t}^1\}$ to denote the received two labels for image $I_t$ at time round $t$ with peer communication.
%We firstly analyze a simplified setting, where the two submitted labels from the peer communication are independent with each other and coming from a same distribution.
%We note that such a simplification is important for investigating this problem.
%In fact, in a later section, we show that there're not much we can improve even when we consider these two labels are not independent.
%Thus: 
%\begin{equation}\label{pr:peer_ability}
%\begin{split}
%\mathbb{P}(y_{I_t}^0 = 1| \theta_{I_t}, \alpha_p, x_t = 1) & =\mathbb{P}(y_{I_t}^1 = 1|\theta_{I_t}, \alpha_p, x_t  = 1)\\
%& = \alpha_{p} \theta_{I_t} + (1-\alpha_{p})(1-\theta_{I_t})
%\end{split}
%\end{equation}
%
%To capture the emprical evidence that the performance of peer workers is better than the single workers, we further assume:
%\begin{align*}
%\alpha_{p} \geq \alpha_{s}
%\end{align*}
%
%
%\subsection{Labelling Cost:}
%Current studies on sequential labelling tasks almost assume the same cost $c$ on each obtained label. 
%In this case, the budget constraint $\mathcal{B}$ can be reduced to the time round constraint $T = \left \lfloor \mathcal{B}/c \right \rfloor$. 
%Under this assupmtion, people can easily formulize this problem as a finite horizon Markov Decision Process. 
%However, it is unnatural for still assuming uniform labelling cost when we have workers which have different working abilities. 
%Particularly, people may need to spend more money on hiring workers who have higher working ability to get more reliable labels.
%Adapted to our setting, the cost $c_p$ for hiring two workers and allowing them to communicate with each other is more than the cost $c_g$ for just hiring single worker.
%Thus, besides the trade-off on which image to query the labels, the requester will face another trade-off on spending more money to get more accurate labels or spending less money to get less accurate labels under the budget constraint.
%Since the cost are not all the same along with all rounds, this problem can then not be formulized as a MDP problem. 
%We formulize this problem as a Constrained MDP which is more challenging on finding optimal allocation policies than traditional MDP problems.
%
%\subsection{Utility Function and Bayesian Setting:}
%Let $H^* = \{k: Z_k = 1\}$ be the positive label set.
%And assume the soft-label $\theta_k$ is consistent with the true label in the sense that $Z_k = 1$ if and only if $\theta_k \geq 0.5$.
%Thus, one can also rewrite positive label set as $H^* = \{k: \theta_k \geq 0.5\}$.
%We adopt the same utility function from~\cite{chen2013optimistic}.
%When the budget is exhausted, the requester needs to infer the positive label set $H_{[\mathcal{B}]}$ to maximize the conditional expected accuracy conditioning the information he collected so far,
%where $[\mathcal{B}]$ means the number of total rounds when budget is exhasuted.
%Define a filteration $\{\mathcal{F}_t\}_{1\leq t\leq [\mathcal{B}]}$, where $\mathcal{F}_t$ is the $\sigma$-algebra generated by the sample path $(x_0, y_{I_0}, ...,x_{t-1}, y_{I_{t-1}}, ...)$,
%Thus, we have
%\begin{align} \label{utility_function}
%H_{[\mathcal{B}]} = \argmax_{H \subset \{1,...,K\}} \mathbb{E}(\sum_{i\in H}\mathbf{1}(i \in H^*) + \sum_{i\notin H}\mathbf{1}(i \notin H^*)|\mathcal{F}_{[\mathcal{B}]})
%\end{align}
%where $\mathbf{1}(.)$ is the indicator function.
%% Let $s^t = \{s_k^t\}_{1\leq k \leq K}$ denote the observed information at round $t$ by the requester, where $s_k^t = \{a_k^t, b_k^t\}$.
%% $s^t$ is also the \textit{markov state} in the following section.
%
%
%We adopt a Bayesian setting to formulate the true label inference in our setting.
%We assume the instances' hidden parameters $\{\theta_k\}_{1\leq k \leq K}$ and workers' type hidden parameters $\{\alpha_j\}_{j = s, p}$ are drawn from known Beta prior distributions:
%\begin{align}
%\theta_k \sim \text{Beta}(a_k^0, b_k^0), \alpha_{j} \sim \text{Beta}(c_{j}^0, d_{j}^0).
%\end{align}
%
%
%
%At each time round $t$, the requester can obtain labels either from single workers or from peer communication workers. 
%In particular, if the requester obtains one label $y_{I_t}$ from a single worker at $t$, then the requester's posterior on hidden parameters can be calculated as follows:
%\begingroup
%\makeatletter\def\f@size{8}
%\begin{align}
%& p(\theta_{I_t}, \alpha_{s}|y_{I_t} = 1, x_t = 0) \\
%& = \frac{\mathbb{P}(y_{I_t} = 1|\theta_{I_t}, \alpha_{s}, x_t = 0)\text{Beta}(a_{I_t}^0, b_{I_t}^0) \text{Beta}(c_{s}^0, d_{s}^0)} {\mathbb{P}(y_{I_t} = 1|x_t = 0)} 
%\end{align}
%\endgroup
%Similarly, the posterior if the requester obtains labels from peer workers is defined as:
%\begingroup
%\makeatletter\def\f@size{8}
%\begin{align}
%& p(\theta_{I_t}, \alpha_{p}|y_{I_t}^0 = 1, y_{I_t}^1 = 1, x_t = 1) \\
%& = \frac{\mathbb{P}^2(y_{I_t} = 1|\theta_{I_t}, \alpha_{p}, x_t = 0)\text{Beta}(a_{I_t}^0, b_{I_t}^0) \text{Beta}(c_{p}^0, d_{p}^0)} {\mathbb{P}(y_{I_t}^0 = 1, y_{I_t}^1 = 1|x_t = 0)} 
%\end{align}
%\endgroup
%where the likelihood $\mathbb{P}(y_{I_t} = 1|\theta_{I_t}, \alpha_{s}, x_t = 0)$ and $\mathbb{P}(y_{I_t}^0 = 1|\theta_{I_t}, \alpha_{p}, x_t = 1)$ are defined in (\ref{pr:single_ability}) and (\ref{pr:peer_ability}).
%
%However, due to the working ability difference of single and peer workers, above posterior won't be following a Beta distributions form. 
%To ensure the requester can still be able to use simple \textit{majority vote} rule to determine the positive set.
%We use approximating posterior inference techniques as introduced in~\cite{chen2013optimistic}.
%
%Particularly, we want the posterior $p(\theta_{I_t}|y_{I_t} = Z_{I_t})$, $p(\alpha_s|y_k^0 = Z_{I_t})$ and $p(\alpha_p|y_k^0 = Z_{I_t})$ could be approximated as Beta distributions:
%\begin{align*}
%p(\theta_{I_t}|y_{I_t} = Z_{I_t}) & \approx \text{Beta}(\tilde{a}_{I_t}(Z_{I_t}), \tilde{b}_{I_t}(Z_{I_t})) \\
%p(\alpha_{j}|y_{I_t} = Z_{I_t}) & \approx \text{Beta}(\tilde{c}_{j}(Z_{I_t}), \tilde{d}_{j}(Z_{I_t}))\quad j = \{s, p\}
%\end{align*}
%where the parameters $\tilde{a}_{I_t}(Z_{I_t})$, $\tilde{b}_{I_t}(Z_{I_t})$, $\tilde{c}_{s}(Z_{I_t})$ and $\tilde{d}_{s}(Z_{I_t})$ can be interprated as pseudo-counts calculated from approxiamtion technique (please refer to \nameref{appendix: posterior_approx} for more details).
%
%We furhter define:
%\begin{align}
%I(a, b) & = \mathbb{P}(\theta \geq 0.5 | \theta \sim \text{Beta}(a, b)) \\
%P_k^t & =\mathbb{P}(i \in H^*|\mathcal{F}_t) = \mathbb{P}(\theta_k \geq 0.5 | s_k^t) = I(a_k^t, b_k^t)
%\end{align}
%Associated with above defined function, we have following corollaries:
%\begin{corollary}
%The expected accuracy on RHS of (\ref{utility_function}) can be written as $\sum_{k=1}^K h(P_k^{[\mathcal{B}]})$ where $h(x) = max(x, 1-x)$.
%\end{corollary}
%
%
%\begin{corollary}
%$I(a, b) > 0.5$ if and only if $a > b$ and $I(a, b) = 0.5$ if and only if $a= b$. 
%Thus, $H_{[\mathcal{B}]} = \{k, a_k^{[\mathcal{B}]} > b_k^{[\mathcal{B}]}\}$.
%\end{corollary}
%Let $u$ denote the allocation policy, to find the optimal policy, we need to solve following optimization problem under the budget constraint:
%\begin{align} \label{obj:objective_function}
%V(\mathcal{S}) & = \sup_{u}\mathbb{E}^u [\mathbb{E}(\sum_{i\in H}\mathbf{1}(i \in H^*) + \sum_{i\notin H}\mathbf{1}(i \notin H^*)|\mathcal{F}_{[\mathcal{B}]})] \\
%& = \sup_u\mathbb{E}^u(\sum_{k=1}^K h(P_k^{[\mathcal{B}]})) \\
%\text{s.t.} & \quad c_s [\mathcal{B}]^s + c_p([\mathcal{B}] - [\mathcal{B}]^s) \leq \mathcal{B} 
%\end{align}
%where $\mathbb{E}^u$ represents the expectation taken over the sample paths $(x_0, y_{I_0}, ...,x_{t-1}, y_{I_{t-1}},...)$ and $[\mathcal{B}]^s$ denotes the total inquiry time rounds for obtaining one label from sinle workers.
%$V(\mathcal{S})$ is the value function given the initial state $s^0$.
%% $[\mathcal{B}]$ is the number of rounds for labels coming from single workers under the budget $\mathcal{B}$.
%
%% \subsection{Bayesian Setting:} 
%% When the budget is exhausted, we need to make an inference about the true label of each instance, i.e., which instance belongs to the positive label set.					
%% Our goal is to determine the optimal allocation sequence $(I_1,...,I_{T})$ (a.k.a. optimal instance allocation policy) and $(x_1, ..., x_T)$ (a.k.a. labelling policy) so that overall accuracy is maximized under the budget constraint, where $T$ is the time round when the budget is exhausted. 
%}
