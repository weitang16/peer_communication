In this section, we first present our experimental study, in which we carefully examine the effects of introducing peer communication
between pairs of workers on the quality of crowdwork through a set of randomized experiments conducted on Amazon's 
Mechanical Turk (MTurk).

\vspace{-5pt}
\subsection{Experimental Design}

\paragraph{Independent vs. Discussion Tasks.}
 
To evaluate the effects of peer communication in crowdsourcing, in our experiments, we considered two ways to structure the tasks:

\squishlist
\item {\em Independent tasks} (tasks without peer communication): 
    In an independent task, workers are instructed to complete the task on their own.
\item {\em Discussion tasks} (tasks with \peer): 
    Inspired by the concept of ``peer instruction'' in educational settings~\cite{Crouch10}, we designed a procedure which guides workers in a discussion task to communicate with each other and complete the task together. 
    Specifically, each worker is paired with another ``co-worker'' on a discussion task. 
    Both workers in the pair are first asked to work on the task and submit their answers independently.
    Then, the pair enters a chat room, where they can see each other's independent answer, and they have two minutes to discuss the task freely. 
    Workers are instructed to explain to each other why they believe their answers are correct. 
    After the discussion, both workers get the opportunity to update
    and submit their final answers.
\squishend

\vspace{-8pt}
\paragraph{Experimental Treatments.}  The most straight-forward experimental design for examining the effects of peer communication would include two treatments, where workers in one treatment are asked to work on a sequence of 
independent tasks while workers in the other treatment complete a sequence of discussion tasks. However, if we adopt such a design, the different nature of independent and discussion tasks (i.e., discussion tasks require more time and effort from workers but
can be more interesting to workers) implies the possibility of observing severe self-selection biases in the experiments (i.e., workers may self-select into the treatment that they
can complete tasks faster or find more enjoyable).

To overcome the drawback of this simple design, we designed our experimental treatments in a way that each treatment consists of the same number
of independent tasks {\em and} discussion tasks, such that neither treatment appears to be obviously more time-consuming or enjoyable. 
In particular, we bundled 6 tasks in each HIT (i.e., Human Intelligence Task on MTurk). 
When a worker accepted our HIT, she was told that there are 4 independent tasks and 2 discussion tasks in the HIT. 
There are two treatments in our experiments: 
\squishlist
\item {\em Treatment 1}: Workers are asked to complete 4 independent tasks followed by 2 discussion tasks.
\item {\em Treatment 2}: Workers are asked to complete 2 discussion tasks followed by 4 independent tasks.
\squishend
Importantly, we did {\em not} tell workers the ordering of the 6 tasks, which helps us to minimize the self-selection biases as the two treatments look the same to workers.
Such design, thus, allows us to examine the effects of peer communication on work quality by comparing the work quality produced in {\em the first two tasks of the HIT} between the two treatments.\footnote{The middle two (independent) tasks of the HIT are not essential for the purpose of examining whether introducing peer communication in a task can affect work quality produced in it; we included them for answering a separate question (i.e., whether peer communication can be used to train workers towards better independent performance) which is not the focus of the current paper. The last two tasks of the HIT were added for balancing the number of tasks of different types in the HIT. Comparisons on work quality produced in the last two tasks should {\em not} be used to estimate the effects of peer communication on work quality, because workers in the last two tasks of the two treatments differed on {\em two} dimensions: the provision of peer communication in the current task, and the previous exposure to tasks with peer communication.}

\vspace{-8pt}
\paragraph{Experimental Tasks.}
We conducted our experiments on three common types of tasks:
\squishlist
\item{\em Image labeling}: In each task, the worker is asked to identify whether the dog shown in an image is a Siberian Husky or a Malamute.
    Dog images we use are collected from the Stanford dogs dataset~\cite{KhoslaYaoJayadevaprakashFeiFei_FGVC2011}. 
\item{\em Optical character recognition (OCR)}:   In each task, the worker is asked to transcribe a vehicle's license plate numbers from photos.
    The photos are taken from the dataset provided by \citet{Shah15doubleornothing}.
\item {\em Audio transcription}: In each task, the worker is asked to transcribe an audio clip which contains about 5 seconds of speech. 
    The audio clips are collected from VoxForge\footnote{\url{http://www.voxforge.org}}.
\squishend

For all 3 types of tasks, we evaluated the work quality in it using the notion of \emph{error}.
In image labeling tasks, we defined 
error as the binary classification error---the error is $0$ for correct labels and $1$ for wrong labels.
For OCR and audio transcription tasks, 
we defined error as the edit distance between the worker's answer and the correct answer, divided by the number of characters in the correct answer.
Naturally, for all tasks, a lower rate of error implies higher work quality.

Notice that designing {\em indirect} interactions between workers following the previous methods (e.g.,~\cite{Drapeau16,Chang17revolt})
might be feasible for tasks like image labeling, but not for the other ones (i.e., OCR and audio transcription) when the space of possible answers become large.
Our experiments thus focus on understanding that for a broader set of tasks where indirect worker interactions may or may not be feasible, whether and how {\em peer communication}---a direct, synchronous, and free-style communication between pairs of workers---can affect
work quality produced in these tasks. 

\vspace{-8pt} 
\paragraph{Experimental Procedure.} To enable synchronous communication between crowd workers, we synchronized the work pace of workers by
dynamically matching pairs of workers and sending them to simultaneously start working on the same sequence of tasks. 
In particular, when each worker arrived at our HIT,
we first checked whether there was another worker in our HIT who didn't have a co-worker yet---if yes, she would be matched to that worker and assigned to the same treatment and task sequence as that worker, and the pair then started working on their sequence of tasks together. 
Otherwise, the worker would be {\em randomly} assigned to one of the two treatments as well as a {\em random} sequence of tasks, and she would be asked to wait for another co-worker to join the HIT for a maximum of 3 minutes. 
%When the next worker shows up and thus be matched to the current worker, both of them will be automatically redirected to the first task in the HIT and they can start working on the HIT simultaneously. 
In the case where no other workers arrived at our HIT within 3 minutes, we asked the worker to decide whether she was willing to complete all tasks in the HIT on her own (and we dropped the data for the analysis but still payed her accordingly) or get a 5-cent bonus to keep waiting for another 3 minutes. 

We provided a base payment of 60 cents for all our HITs.
In addition to the base payments, workers were provided with the opportunity to earn performance-based bonuses, that is, workers can earn a bonus of 10 cents in a task if the final answer they submit for that task is correct. 
Our experiment HITs were open to U.S. workers only, and each worker was only allowed to take one HIT for each type of tasks.

\begin{figure}[t]
   \centering
   \includegraphics[scale=0.3]{figure1.png}
   \vspace{-15pt}
   \caption{Comparisons of work quality produced in tasks with or without peer communication. Error bars indicate the mean $\pm$ one standard error.}
   \vspace{-15pt}
   \label{fig:expResult}
\end{figure}

\vspace{-5pt} 
\subsection{Experimental Results} 
In total, we had 388, 382, and 250 workers who successfully formed pairs and completed the image labeling, OCR, and audio transcription tasks in our experiments, respectively.

%\vspace{-8pt}
%\paragraph{Peer Communication Improves Work Quality.}
In Figure~\ref{fig:expResult}, we plot the average error rate for workers' final answers in the first two tasks of Treatment 1 HITs (or Treatment 2 HITs) using white (or black) bars.
%Results are shown in Figure~\ref{fig:h1}. 
Visually, it is clear that for all three types of tasks, the work quality is higher in discussion tasks (i.e., the first two tasks of Treatment 2 HITs) when workers are able to communicate with others about the work, compared to that in independent tasks (i.e., the first two tasks of Treatment 1 HITs)
where workers need to complete the work on their own. We further conduct two-sample t-tests to check the statistical significance of the differences, and 
p-values for image labeling, OCR and audio transcription tasks are $2.42\times 10^{-4}$, $5.02\times 10^{-3}$, and $1.95\times 10^{-11}$ respectively, 
suggesting that introducing peer communication in crowdsourcing indeed improve the work quality produced significantly.\footnote{As a secondary result, comparing work quality produced in the middle two independent tasks between the two treatments suggest that previous interactions with other workers on the same type of tasks does {\em not} help workers to improve their independent performance, at least for short interactions as the ones we operationalized in our \peer procedure (i.e., two minutes). }
%\cj{maybe add a few sentences about peer communication won't impact workers' future performance}


