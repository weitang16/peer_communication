Crowdsourcing has gained increasing popularity recently as a scalable data collection tool for various purposes, such as obtaining labeled data for training
machine learning algorithms and getting high-quality yet cheap transcriptions for audio files. On a typical crowdsourcing platform like Amazon Mechanical Turk (MTurk) or Figure Eight, task requesters can post small jobs (e.g., image labeling or audio transcription tasks) as ``microtasks'' along with the specified payment for completing each task. Workers then can browse all available tasks on the platform and decide which ones to work on. It is often assumed that crowd workers complete tasks on crowdsourcing platforms {\em independently}. As a result, a classical quality control method that requesters often adopt is to hire multiple workers to work on the same task and later combine these workers' answers together. Such practice inspires a substantial amount of crowdsourcing research in the AI community to study how to make better use of the independent workers. For example, a rich body of research has explored how independent contributions from multiple workers can be effectively aggregated by inferring task difficulty, worker skill and the correct answer simultaneously~\cite{raykar10,cholleti08,whitehill09,zheng2017truth}. Moreover, given a limited budget, researchers have further examined how to intelligently decide the number of independent workers needed for each task at the first place~\cite{chen2013optimistic,li2016crowdsourcing,gurari2017crowdverge}.

\ignore{Requesters who have a large amount of work at hand (e.g., label 1,000 images or transcribe 1,000 audio clips) can post such work
on crowdsourcing platforms like Amazon's Mechanical Turk (MTurk) as a batch of ``microtasks.'' Workers on the platforms can then work on each
of these tasks for a short amount of time in exchange for some financial rewards, and requesters typically recruit multiple workers to work
on the same task {\em independently} for quality assurance. A substantial amount of research in crowdsourcing, thus, focuses on how to effectively aggregate independent contributions from multiple workers~\cite{raykar10,cholleti08,whitehill09}, and how to intelligently decide the number of independent workers needed for each task at the first place~\cite{chen2013optimistic,li2016crowdsourcing,gurari2017crowdverge}.}
 
However, the validity and value of this independence assumption in crowdsourcing has been challenged recently. Through a combination of ethnographic and experimental methodologies, researchers have found that crowd workers, in fact, communicate and collaborate with each other through both online forums (like TurkerNation\footnote{\url{http://turkernation.com/}} and MTurkCrowd\footnote{\url{https://www.mturkcrowd.com/}}) and one-on-one channels~\cite{martin2014being,gray2016crowd,gupta2014turk,irani2013turkopticon,salehi2015we,yin2016communication}. 
Different from such collaboration which is organically arisen within the crowd and mostly about exchanging meta-level information related to crowd work (e.g., how to find well-paid tasks), an increasing number of studies in the human-computer interaction community have started to 
design certain level of interactions between workers in their actual work, which is shown to improve crowdsourcing outcomes in many cases. 
For example, various {\em workflows} are developed to coordinate workers to work on {\em different} subtasks and interact with each other through the pre-defined input-output handoffs~\cite{Little10,noronha2011platemate,bernstein2010soylent,Kittur:11,Kulkarni:12,chilton2013cascade,kim2014crowdsourcing,Retelny:14}, which enable the crowd to jointly complete complex tasks. 

More recently, worker interactions are further introduced between workers of the {\em same} task:~\citet{Drapeau16} and~\citet{Chang17revolt} showed that in image/text labeling tasks,
workers can improve their labeling accuracy when {\em indirect} interactions---in the form of showing each worker 
the alternative answer and associated justification produced by another worker who works on the same task---are enabled, and~\citet{Mike:18} observed that in text classification tasks, worker performance increases when they can debate their answers through {\em direct, real-time} deliberation with one another. While these research show the promise of an alternative way to structure crowd work that leads to higher performance, they also raise a number of open questions. 

First, on the ``micro'' level, it is empirically important to examine whether adding interactions between workers working on the same task would lead to increase in work quality for {\em individual} tasks of {\em different} types, especially for those tasks with a large number of possible answers (rather than just a few options as in image labeling and text classification tasks). Indeed, when the number of possible answers in a task becomes large, workers may hardly agree with each other so it is unclear whether interactions between them would be meaningful and effective. It is also impractical for workers to argue against all alternative answers during their interactions, which may imply the need for new formats of interactions beyond providing justification and argumentation.
%It is also interesting to explore whether simply having interactions in some tasks would allow workers to learn from each other through the interaction and thus improve their independent performance on future tasks of similar type. 

Furthermore, from a more ``macro'' point of view, requesters typically have a {\em large batch} of tasks at hand and solicit answers from multiple workers for each task, and usually their goal is to optimize their {\em overall} utility such as maximizing the quality obtained across {\em all} the tasks under a fixed budget.
Yet, compared to independent work, allowing worker interactions in a task can bring up not only work quality improvement in that task, but also higher correlation in workers' answers and higher cost. Thus, for requesters to make better use of worker interactions, a critical problem to address is that given a limited budget, {\em whether} and {\em when} should worker interactions be used in each task in their batch, such that after combining the possibly correlated answers together for each task, the work quality for the entire batch of tasks is maximized when the budget is exhausted. 

\ignore{
More recently, researchers have started to explore the possibility of removing this independence assumption and enable worker collaboration in crowdsourcing.
One typical approach is to design {\em workflows} that coordinate crowd workers for solving complex tasks. 
%For example, consider the task of calculating the total calories of a meal in a picture. 
%While this task is often considered to be too complex as a microtask for a single worker to complete, 
%it is shown that we can design workflows that decompose this task into multiple microtasks (e.g., finding food items in a picture, identifying ingredients of each item, and answering the calories for a single ingredient) 
%and recruit a team of workers to complete this task~\cite{Noronha:11}. 
Specifically, a workflow involves decomposing a complex task into multiple simple microtasks, and workers are then asked to work on {\em different} microtasks. 
Since decomposed microtasks may depend on each other (e.g., the output of one task may be used as the input for another),
workers are implicitly interacting with one another and are not working independently.
Along this line, there has been a great amount of research demonstrating that relaxing the worker 
independence assumption could enable us to go beyond microtasks and solve various complex tasks using crowdsourcing~\cite{Bigham10,Kittur:11,Kulkarni:12,Retelny:14}.
%However, worker interactions enabled by workflows are often implicit, carry limited , 
%without direct and free-style communications between workers.


Another line of research has demonstrated that even when workers are working on the {\em same} microtask, 
enabling some form of {\em structured interactions} between workers could be beneficial as well. 
In particular, ~\citet{Drapeau16} and ~\citet{Chang17revolt} has shown that, in labeling tasks,
if workers are presented with alternative answers and the associated arguments, 
which are generated by other workers working on the same tasks,
they can provide labels with higher accuracy. 
These results, again, imply that including worker interactions could have positive impacts on crowdwork.
%the quality of crowdwork even when workers are working on the same microtask.

%More recently, researchers have started to explore the possibility of removing the independence property of crowdwork.
%For example, \citet{Drapeau16} and ~\citet{Chang17revolt} have shown that in labeling tasks,
%workers can improve their labeling accuracy when some form of {\em indirect} and {\em structured} interactions are enabled (e.g., showing one worker the alternative answer
%and associated argument generated by a previous worker who has worked on the same task; 
%workers thus ``interact'' with one another through their answers and arguments). While these results show the promise of 
%enabling worker interactions in crowdwork, the particular formats of worker interactions used in these studies
%are quite task-specific. Therefore, it is unclear whether such interaction methods can be easily adapted to other tasks and whether
%similar findings can be obtained in different contexts.
%

In both these lines of research, however, interactions between workers are {\em indirect} and {\em constrained} by the particular format of 
information exchange that is pre-defined by requesters (e.g., the input-output handoffs in workflows, the elicitation of arguments for workers' answers).
Such form of worker interactions can be context-specific and may not be easily adapted to different contexts. For example, it is unclear whether
presenting alternative answers and arguments would still improve worker performance for tasks other than labeling, where it can be hard for workers to provide a simple
justification for their answers.

On the other hand, a more general form of interaction that can be easily adopted in various contexts is to allow workers 
of the same task to {\em directly} interact with each other while they are working on the task.   
Inspired by the concept of peer instruction in education~\cite{Crouch10}, in this paper, we study one specific kind of direct interaction, which we refer to as {\em \peer}.
In particular, we operationalize \peer as a procedure where a {\em pair} of workers working on the same task are asked to first provide an independent answer each, 
then freely discuss the
task with each other, and finally provide an updated answer after the discussion. }

%\cj{Cite papers outside of crowdsourcing. Do we know any papers that say communication doesn't necessarily improve work?}

Therefore, in this paper, we attempt to answer these two questions. In particular, inspired by the concept of peer instruction in education~\cite{Crouch10}, in this work, we focus on studying a specific format of worker interactions that we refer to as {\em \peer}---a {\em pair} of workers working on the same task are asked to first provide an independent answer each, then freely discuss the task with each other, and finally provide an updated answer, again independently, after the discussion. Compared to worker interaction formats used in the early research (e.g., justification and argumentation), we consider \peer as a kind of direct and synchronous interaction that can be generalized to different types of tasks more easily. Our goal is to better understand not only whether and how \peer would affect the outcome of crowd work for various types of tasks, but also how requesters can use algorithmic approaches to better utilize the potential benefits brought up by \peer.  

To understand the effects of \peer on crowd work, 
%We first ask whether introducing peer communication in crowdwork can bring up benefits like improvement in work quality. 
%To this end, 
we design and conduct randomized experiments with three different types of tasks: image labeling, optical character recognition, and audio transcription.
%, and the number of possible answers in these tasks varies from two (i.e., image labeling) to infinitely many (i.e., audio transcription).
Our results suggest that for all types of tasks in our experiments, regardless of how large the number of possible answers in the task is,
we have consistently observed an increase in work quality when workers can talk with their peers in the task compared to workers who work independently. Yet, we do not observe any spillover effects of such quality improvement when workers who have engaged in \peer are asked to work on similar tasks again independently.
%workers with \peer perform significantly better than workers who work independently. 
%The results are robust and consistently observed for all three types of tasks.

Moreover, to examine how \peer can be better utilized, we propose an algorithmic framework %based on constrained Markov decision process %\wt{CMDP consistency} 
to help requesters 
make {\em online decisions} on whether and when to use \peer for each task in their batch, with the goal of maximizing
the overall work quality produced in all tasks given a budget constraint.
One of the key challenges here is how to infer the correct answer for a task given multiple answers solicited from workers, where some of them may be produced following the \peer procedure and thus may be correlated.
To this end, we introduce the notions of {\em meta-workers} and {\em meta-labels} to describe a pair of workers who have engaged in a task with \peer and the pair of answers produced by them. Such notions enable us to characterize the possible correlation in data,  
%that are generated through tasks with \peer, 
which further allow us
to solve the requester's online decision-making problem by modeling it as a constrained Markov decision process. 

We evaluate the effectiveness of 
the proposed algorithmic approach on real data collected through our experimental study. Results show that using our approach to decide the usage of \peer in tasks, the requester can
achieve higher overall quality across all his tasks when the budget is exhausted, compared to when baseline approaches are adopted where 
\peer is always used in all tasks or never used in any of the tasks, or when correlation in data is not explicitly considered. In addition, through two sets of simulated experiments, we further examine how the proposed algorithmic approach performs in various scenarios when the differences in work quality and cost between hiring pairs of communicating workers and hiring independent workers vary, and when answers produced by pairs of communicating workers are correlated to different extent.

In summary, we make the following contributions:
\squishlist
\item We introduce \peer, a general mechanism adapted from the concept of peer instruction in education for including worker interactions in crowd work. 
\item We empirically show that on different types of tasks, compared to independent work, \peer consistently leads to a 32\%--47\% improvement in work quality for individual tasks.
\item We propose an algorithmic framework for helping requesters dynamically decide whether and when to use \peer for each task in their batch so as to maximize the overall quality obtained across all tasks given a budget constraint. To the best of our knowledge, this is the first work that enable requesters to algorithmically control the deployment of worker interactions to maximize their utility.
\item Through evaluations on both real data from crowd workers and synthetic data, we demonstrate that compared to baseline approaches, 
using our proposed algorithm to determine the deployment of \peer leads to higher requester utility.
% of our proposed algorithm against a few baseline approaches using both real data collected from crowd workers and synthetic data, and find that following the proposed algorithm to determine the usage of \peer in each task, requesters can obtain significantly higher overall work quality given the same budget.  
\squishend

\ignore{
Next, we explore how to effectively utilize \peer in crowdsourcing. Specifically, similar to questions that have been asked for independent crowdwork,
we ask: (1) How should the requester {\em aggregate} multiple contributions on a task if some of them are produced by pairs of workers following the peer communication procedure? (2) Given that tasks with peer communication
may bring up work of higher quality with higher cost compared to tasks where workers work independently, how should the requester intelligently decide {\em whether} and {\em when} peer communication
is needed for each task in his batch at the first place?


In this paper, we answer these questions for binary classification tasks. With respect to aggregation, not surprisingly,
we find that data generated through tasks with peer communication is {\em correlated}. 
To carefully leverage such correlated data, we derive an aggregation method using weighted majority voting.
We then show that, with the appropriately-chosen weights, 
weighted majority voting leads to the maximum likelihood estimation.
%Our method suggests that in general, the requester needs to consider the covariance between answers from workers who have communicated with one another for effective aggregation.
%However, interestingly, we find that
%in \peer where only {\em two} workers are involved in the communication, majority voting is still the optimal aggregation method.
 
Moreover, we propose an algorithmic framework based on constrained Markov decision process %\wt{CMDP consistency} 
to help requesters
adaptively learn to decide whether and when to use \peer for each task in his batch. We evaluate the effectiveness of 
the proposed algorithmic approach on real data collected through our experimental study. Results show that using our approach, requesters 
achieve higher overall quality across all tasks in his batch within a given budget, compared to when baseline approaches are adopted where the requester
always stick with the traditional method of recruiting workers to work on each task independently.  }
%\peer is used on all tasks or none of the tasks.

%to help requesters We next examine when and whether should we deploy \peer for crowdsourcing data collection.
%Since enabling \peer incurs higher costs, it might not always be beneficial to deploy \peer.
%To examine the effect of \peer in crowdsourcing data collection with a budget constraint,
%We propose a framework based on constrained Markov Decision Process.
%Our proposed algorithm adaptively learn to decide when and which task should we use \peer.
%Experiments conducted on real data demonstrate that our approach algorithm outperforms baseline approaches that do not utilize \peer.


%\cj{Maybe explicitly write down the contributions again.}
