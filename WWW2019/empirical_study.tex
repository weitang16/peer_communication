In this section, we first present our experimental study, in which we carefully examine the effects of introducing \peer between pairs of workers
on the quality produced in individual tasks through a set of randomized experiments conducted on Amazon's Mechanical Turk (MTurk). In particular,
we ask:
\squishlist
    \item \textbf{Question 1 (Q1)}: 
    Do workers produce higher work quality in tasks with \peer compared to that in tasks where workers work independently?
    %Workers can produce higher work quality in tasks with peer communi- cation than that in tasks where they work independently.
\squishend

Previous studies on the effects of adding worker interactions in image and text classification tasks~\cite{Drapeau16,Chang17revolt,Mike:18} seem to imply a positive answer for Q1. Compared to these studies, our study has two key differences that warrant a re-examination of Q1: (1) the main format of interaction in \peer is a {\em synchronous, free-form chat} rather than required justification and argumentation; (2) we consider different types of tasks beyond classification, especially tasks with a large number of possible answers so workers can hardly agree with each other or argue against all alternative answers. 
Moreover, we are also interested in examining whether there is any ``spillover'' effects of the impact of \peer on work quality. Specifically:   

\squishlist
 \item \textbf{Question 2 (Q2)}: 
    Do workers produce higher independent work quality after engaging in similar tasks
    with \peer, compared to workers who always complete tasks on their own?
    %workers' performance on the task does not depend on whether they have completed on the same type of tasks with \peer before.
\squishend

Both positive and negative answers might be possible for Q2: On the one hand, if communication between workers in tasks allow them to resolve misconception about the tasks or learn useful problem-solving strategies from each other, we might expect a positive answer; on the other hand, if the benefits of \peer are mostly due to workers being able to exchange their confidence levels on a task and eventually converge to the more confident answer~\cite{bahrami2010optimally}, the answer for Q2 would likely be negative.

%We design and conduct a set of online experiments on Amazon Mechanical Turk (MTurk).
%We examine the two hypothesis on three types of microtasks that commonly appear on crowdsourcing platforms, including image labeling, optical character recognition (OCR), and audio transcription.

%We detail our experimental design in the following.
%In the following, we describe our experiement design, including the data collection process, the approach we examine the two hypotheses using the collected data, and practical concerns in running the experiments.

\subsection{Independent Tasks vs. Discussion Tasks}

%We are interested in understanding whether allowing workers to work in pairs and directly communicate with each other about the {\em same} tasks would lead to work of better quality 
%compared to that when workers complete the work independently.
%To do so, in our experiments, we consider both tasks {\em with} peer communication and tasks {\em without} peer communication:
%To evaluate the effects of peer communication in crowdsourcing, 
In our experiments, we considered two ways to structure the tasks:

\squishlist
    \item \textbf{Independent tasks} (tasks without peer communication). 
    In an independent task, workers are instructed to complete the task on their own. 

    \item \textbf{Discussion tasks} (tasks with \peer). 
    Inspired by the concept of ``peer instruction'' in educational settings~\cite{Crouch10}, we designed a procedure which guides workers in a discussion task to communicate with each other and complete the task together. 
    Specifically, each worker is paired with another ``co-worker'' on a discussion task. 
    Both workers in the pair are first asked to work on the task and submit their answers independently.
    Then, the pair enters a chat room, where they can see each other's independent answer. 
    Workers are instructed to freely discuss the task with their co-workers for two minutes, for example, they can 
    explain to each other why they believe their answers are correct. 
    After the discussion, both workers get the opportunity to independently update
    and submit their final answers.
\squishend

\begin{figure*}[t]
   \centering
   \includegraphics[width=1.8\columnwidth, keepaspectratio]{figures/exp-design-2.pdf}
   \vspace{-15pt}
   \caption{The two experimental treatments. 
        %Each worker is randomly assigned to one treatment. 
        %Before starting the HIT, they are told that there will be four tasks with independent work and two tasks with Peer instruction. 
       % However, they are not aware of the ordering of the tasks.
        This design enables us to examine whether \peer improves the quality of crowd work (by comparing work quality in Session 1) and if so, does the improvement spill over to the following independent tasks (by comparing work quality in Session 2), while not creating significant differences between the two treatments (by adding Session 3 to make the two treatments containing equal number of independent and discussion tasks).
        }
   \label{fig:exp-design}
   \vspace{-10pt}
\end{figure*}

\subsection{Experimental Treatments}
\label{two-treatment-design}

The most straight-forward experimental design would include two treatments, where workers in one treatment are asked to work on a sequence of 
independent tasks while workers in the other treatment complete a sequence of discussion tasks.
However, if we adopt such a design, the different nature of independent and discussion tasks (e.g., discussion tasks require more time and effort from workers but can be more interesting to workers) implies the possibility of observing severe self-selection biases in the experiments (i.e., workers may self-select into the treatment that they
can complete tasks faster or find more enjoyable).



To overcome the drawback of this simple design, we design our experimental treatments in a way that each treatment consists of the same number
of independent tasks {\em and} discussion tasks, so neither treatment appears to be obviously more time-consuming or enjoyable. 
Figure~\ref{fig:exp-design} illustrates the two treatments used in our experiments.
In particular, we bundle 6 tasks in each HIT (i.e., Human Intelligence Task on MTurk). 
When a worker accepted our HIT, she was told that there are 4 independent tasks and 2 discussion tasks in the HIT. 
There are two treatments in our experiments: 
\squishlist
\item{\em Treatment 1}:  Workers are asked to complete 4 independent tasks followed by 2 discussion tasks.
\item{\em Treatment 2}:  Workers are asked to complete 2 discussion tasks followed by 4 independent tasks.
\squishend
%in Treatment 1, workers are asked to complete 4 independent tasks followed by 2 discussion tasks, while workers in Treatment 2 first work on 2 discussion tasks and then complete 4 independent tasks.
Importantly, we did {\em not} tell workers the ordering of the 6 tasks, which helps us to minimize the self-selection biases as the two treatments look the same to workers. We refer to the first, middle, and last two tasks in the sequence as Session 1, 2, 3 of the HIT, respectively. Thus, we can answer Q1 by comparing the work quality produced in Session 1 between the two treatments, 
%Intuitively, observing higher work quality in Session 1 of Treatment 2 would imply that \peer can enhance work quality above the level of independent worker performance.
while a comparison of work quality in Session 2 between the two treatments would allow us to answer Q2.
%Similarly, we can test H2 by comparing the work quality in Session 2 (i.e. the middle two tasks of the HIT) between the two treatments.
%H2 is supported if the work quality in Session 2 of Treatment 2 is also higher than that of Treatment 1, which would suggest that after communicating with peers, 
%workers are able to produce higher quality {\em in their independent work} for the same type of tasks.
Finally, Session 3 is used for balancing the number of independent and discussion tasks in each HIT. 

%To overcome the drawback of this simple design, we design our experimental treatments in a way that each treatment consists of the same number
%of independent tasks {\em and} discussion tasks, such that neither treatment appears to be obviously more time-consuming or enjoyable. 
%%Figure~\ref{fig:exp-design} illustrates the two treatments used in our experiments.
%In particular, we bundle 6 tasks in each HIT\footnote{HIT stands for Human Intelligence Task, and it refers to one unit of job on MTurk that a worker can accept to work on.}. 
%When a worker accepts our HIT, she is told that there are 4 independent tasks and 2 discussion tasks in the HIT. 
%There are two treatments in our experiments: 
%\begin{itemize}
%%\squishlist
%\item \textbf{Treatment 1}: Workers are asked to complete 4 independent tasks followed by 2 discussion tasks.
%\item \textbf{Treatment 2}: Workers are asked to complete 2 discussion tasks followed by 4 independent tasks.
%%\squishend
%\end{itemize}
%
%Importantly, we did {\em not} tell workers the ordering of the 6 tasks, which helps us to minimize the self-selection biases as the two treatments look the same to workers.
%Such design, thus, allows us to examine the effects of peer communication on work quality by comparing the work quality produced in {\em the first two tasks of the HIT} between the two treatments.\footnote{The middle two (independent) tasks of the HIT are not essential for the purpose of examining whether introducing peer communication in a task can affect work quality produced in it; we included them to examine whether peer communication has \emph{spillover} effect (i.e., whether workers perform better for the same types of tasks without peer communication after completing tasks with peer communication).
%The last two tasks of the HIT were added for balancing the number of tasks of different types in the HIT. 
%Comparisons on work quality produced in the last two tasks should {\em not} be used to estimate the effects of peer communication on work quality, because workers in the last two tasks of the two treatments differed on {\em two} dimensions: the provision of peer communication in the current task, and the previous exposure to tasks with peer communication.}
%

%Given the way we design the treatments, we can examine H1 by comparing the work quality produced in Session 1 (i.e. the first two tasks of the HIT) between the two treatments. 
%Intuitively, observing higher work quality in Session 1 of Treatment 2 would imply that \peer can enhance work quality above the level of independent worker performance.
%Similarly, we can test H2 by comparing the work quality in Session 2 (i.e. the middle two tasks of the HIT) between the two treatments.
%H2 is supported if the work quality in Session 2 of Treatment 2 is also higher than that of Treatment 1, which would suggest that after communicating with peers, 
%workers are able to produce higher quality {\em in their independent work} for the same type of tasks.
%Finally, Session 3 (i.e. the last two tasks of the HIT) is used to ensure that the two treatments require similar amount of work from workers. 
%\ignore{It's important to note that we have decided {\em not} to draw any causal conclusions using data collected in Session 3 in the experimental design phase, because Session 3 of the two treatments differs
%on both whether workers have worked in pairs in previous tasks and whether 
% as and the data collected in these two tasks is discarded\footnote{On a side note, analyzing the data collected in the last two tasks leads to conclusions that are consistent with our findings (about H1) reported below, and including such data only strengthens our results. 
%However, since we have decided not to use it in the experiment design phase,
%we do not include the data in our analyses.}}
%

\subsection{Experimental Tasks}

We conducted our experiments on three types of tasks: %image labeling, optical character recognition (OCR), and audio transcription. 
%These tasks are all very common types of tasks on crowdsourcing platforms, hence experimental results on these tasks allow us to understand how \peer affects the quality of crowdwork for various kinds of typical tasks. 
%We are interested in understanding the effects of peer instruction for these tasks. 
%For each type of task, we intentionally pick difficult problem instances such that it is hard for individual workers to achieve perfect performance. 

\squishlist
    \item \textbf{Image labeling.}
    In each task, the worker is asked to identify whether the dog shown in an image is a Siberian Husky or a Malamute.
    Dog images we use are collected from the Stanford Dogs dataset~\cite{KhoslaYaoJayadevaprakashFeiFei_FGVC2011}. 
    %Since the task can be difficult for workers who are not familiar with dog species,
    %we provide workers with a table summarizing the characteristics of each dog species, as shown in Figure~\ref{fig:dog-instruction}.
    %Workers can get access to this table at anytime when working on the HIT.
    %\my{Do we need to explain somewhere that workers also see this table in discussion? Maybe we can remove this sentence and the last sentence in the audio transcription together after we describe the three types of tasks.}

    \item \textbf{Optical character recognition (OCR).}
    In each task, the worker is asked to transcribe a vehicle's license plate numbers from photos.
    The photos are taken from the dataset provided by \citet{Shah15doubleornothing}.
    %\footnote{Their files are from \url{http://www.coolpl8z.com/}},
    %There are various lighting conditions on the photos, so it is difficult for individual workers to transcribe the license numbers perfectly.
    %and some examples are shown in Figure~\ref{fig:ocr}.

    \item \textbf{Audio transcription.}
    In each task, the worker is asked to transcribe an audio clip which contains about 5 seconds of speech. 
    The audio clips are collected from VoxForge\footnote{\url{http://www.voxforge.org}}.
    %We intentionally choose clips from speakers with heavy accents to increase the difficulty of the task. 

\squishend

We decided to conduct our experiments on these three types of tasks for two main reasons: First, these tasks are all very common types of tasks on crowdsourcing platforms~\cite{difallah2015dynamics}, so experimenting with them would allow us to better understand the effects of \peer on typical kind of crowd work. Second, in terms of the number of possible answers, these tasks span a wide spectrum from two (image labeling) to infinitely many (audio transcription), enabling us to both confirm the effects of \peer in tasks with just a few possible answers and explore its effects in tasks with many possible answers.
%where it's unlikely for workers to completely agree with each other and it's also impractical for workers to argue against all alternative answers. 
As a final note, tasks we bundled in the same HIT had a certain degree of similarity\footnote{For example, image labeling tasks are all about the key concept of distinguishing Siberian Husky from Malamute, OCR tasks have similar image quality, and audio transcription tasks contain similar accents.}, hence a spillover  effect of \peer on work quality is not impossible as knowledge/strategy that workers may learn in one task can potentially be transferred to another task.

%although we did not explicitly design the \peer procedure as a training method, \peer may affect worker's future independent performance as workers may be able to learn from each other through their interactions.  

\ignore{
\begin{figure}[t]
   \centering
   \includegraphics[width=1.0\columnwidth, keepaspectratio]{figures/dog-instruction.pdf}
   \vspace{-25pt}
   \caption{The instruction of the image labeling task.}
   \label{fig:dog-instruction}
\end{figure}


\begin{figure}[t]
   \centering
   \includegraphics[width=0.8\columnwidth, keepaspectratio]{figures/ocr-example.pdf}
   \caption{Examples of photos used in the OCR task. }
   \label{fig:ocr}
\end{figure}


Unlike in the image labeling task, we do not provide additional instructions for the OCR and audio transcription tasks. 
Indeed, for some types of crowdwork, it is difficult for requesters to provide detailed instructions. 
However, the existence of detailed task instruction may influence the effectiveness of \peer (e.g., workers in the image labeling tasks can simply discuss with their co-workers whether each distinguishing feature covered in the instruction is presented in the dog image). 
Thus, examining the effect of \peer on work quality for different types of tasks, where detailed instruction may or may not be possible,
helps us to understand whether such effect is dependent on particular elements in the design of the tasks. 
}
	
\subsection{Experimental Procedure}

%Implementing the peer instruction process in crowdsourcing settings 
%Introducing direct communication between pairs of workers on the same tasks requires us to synchronize the work pace of pairs of workers, which is quite challenging 
Enabling synchronous work among crowd workers is quite challenging,
as discussed in previous research on real-time crowdsourcing~\cite{Bigham10,Bernstein11}.
We address this challenge by dynamically matching pairs of workers and sending them to simultaneously start working on the same sequence of tasks. 
In particular, when each worker arrives at our HIT,
we first checked whether there was another worker in our HIT who didn't have a co-worker yet---if yes, she would be matched to that worker and assigned to the same treatment and task sequence as that worker, and the pair then started working on their sequence of tasks together.
Otherwise, the worker would be {\em randomly} assigned to one of the two treatments as well as a {\em random} sequence of tasks, and she would be asked to wait for another co-worker to join the HIT for a maximum of 3 minutes. 
%We will prompt the worker with a beep sound if another worker indeed arrives at our HIT during this 3-minute waiting period. 
%Once we successfully match a pair of workers, both of them will be automatically redirected to the first task in the HIT and they can start working on the HIT simultaneously. 
In the case where no other workers arrived at our HIT within 3 minutes, we asked the worker to decide whether she was willing to complete all tasks in the HIT on her own (and we dropped the data for the analysis but still paid her accordingly) or get a 5-cent bonus to keep waiting for another 3 minutes. 

We provided a base payment of 60 cents for all our HITs.
In addition to the base payments, workers were provided with the opportunity to earn performance-based bonuses, that is, workers can earn a bonus of 10 cents in a task if the final answer they submit for that task is correct. 
Our experiment HITs were open to U.S. workers only, and each worker was only allowed to take one HIT for each type of tasks.
